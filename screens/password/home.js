import React, { Component } from "react";
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    Image
} from "react-native";

import Theme from '../../theme/style'
import BackButton from '../../theme/back'
import FormStyle from '../../theme/form'
const width = Dimensions.get("window").width
import isIphoneX from '../../common/iphonex'
// import axios from 'axios';
// import config from '../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`
// import * as storage from "../../redux/actions/storage";
import { DropDownHolder } from '../../common/dropalert'

class PasswordScreen extends Component {


    // validateInput(input) {
    //     if (input === undefined)
    //         return false
    //     else if (input === '')
    //         return false
    //     else if (input.trim() === '')
    //         return false
    //     else if (input === 0)
    //         return false
    //     else
    //         return true
    // }
    // validateAndSetAttribute(value, attribute) {
    //     const valid = this.validateInput(value)
    //     const borderBottomColor = !valid ? 'red' : '#E8E8E8';
    //     attribute.setNativeProps({
    //         style: { borderBottomColor }
    //     });
    //     return valid
    // }

    constructor(props) {

        super(props)
        this.state = {
            currentPassword: '',
            newPassword: '',
            renewPassword: '',
            userLoginData: '',
            disable: '',
            isbutton: true
        };
        // this._currentPasswordEntry = undefined;
        // this._newPasswordEntry = undefined;
        // this._renewPasswordEntry = undefined;
        // this.keyboardBehavior = "padding";
        // this.getstorageData();
    }
    // async getstorageData() {
    //     await storage.storageGet("user").then((result) => {
    //         if (result != null) {
    //             this.setState({ userLoginData: JSON.parse(result) })
    //         } else {
    //             console.log("error")
    //         }
    //     });

    // }
    checkCurrentPassword() {
        const { currentPassword, userLoginData } = this.state;
        var data = {
            action: "checkPassword",
            userid: userLoginData.id,
            password: currentPassword,
            appsecret: appsecret
        }
        axios.post(BASE_URL + 'resetpassword.php', data,
        ).then(result => {
            if (result.data.status == "error") {
                DropDownHolder.alert('error', 'Password Wrong', 'Current Password doesnot match with our database!')
            }
        }),
            err => {
                // DropDownHolder.alert('error', '', 'something went wrong')
                console.log("errror",err)
            }
    }
    checkrePassword() {
        const { newPassword, currentPassword, renewPassword, isbutton } = this.state;
        if (currentPassword && newPassword && renewPassword) {

            if (renewPassword !== newPassword) {
                // DropDownHolder.alert('error', '', 'new password and confirm password doesnot match')
                this.setState({ disable: true, isbutton: true })
            }
            else {
                this.setState({ disable: false, isbutton: false })
            }
        }

    }
    save() {
        const { navigation } = this.props;
        const { currentPassword, newPassword, renewPassword, userLoginData } = this.state;
        const formIsValid =
            this.validateAndSetAttribute(currentPassword, this._currentPasswordEntry) &
            this.validateAndSetAttribute(newPassword, this._newPasswordEntry) &
            this.validateAndSetAttribute(renewPassword, this._renewPasswordEntry);
        if (formIsValid === 1) {
            var data = {
                action: "resetPassword",
                userid: userLoginData.id,
                password: newPassword,
                appsecret: appsecret
            }
            console.log("data", data)
            axios.post(BASE_URL + 'resetpassword.php', data,
            ).then(result => {
                console.log("result", result.data)
                if (result.data.status == "success") {
                    DropDownHolder.alert('success', 'Successful', 'Your password has been changed successfully.')
                    navigation.navigate('ProfileScreen');
                }
                else {
                    DropDownHolder.alert('error', '', 'something went wrong')
                }
            }),
                err => {
                    // DropDownHolder.alert('error', '', 'something went wrong')
                    console.log("error",error)
                }
        } else {
            DropDownHolder.alert('error', '', 'Please enter your details.')
        }
    }

    render() {
        const { currentPassword, disable, isbutton, newPassword, renewPassword } = this.state;
        return (

            <View style={Styles.container,{width:300}}>
                <Text style={Styles.title}>Change Password</Text>
                <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                    <TextInput placeholderTextColor={Theme.secondaryColor}
                        placeholder={'Current Password'}
                        autoCorrect={false}
                        // ref={(ref) => this._currentPasswordEntry = ref}
                        maxLength={200}
                        secureTextEntry
                        onChangeText={currentPassword => this.setState({ currentPassword }, () => this.checkrePassword())}
                        onEndEditing={() => this.checkCurrentPassword()}
                        style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                </View>

                <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                    <TextInput placeholderTextColor={Theme.secondaryColor}
                        placeholder={'New Password'}
                        autoCorrect={false}
                        // ref={(ref) => this._newPasswordEntry = ref}
                        maxLength={16}
                        minLength={6}
                        secureTextEntry
                        onChangeText={newPassword => this.setState({ newPassword }, () => this.checkrePassword())}
                        style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                </View>
                <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                    <TextInput placeholderTextColor={Theme.secondaryColor}
                        placeholder={'Confirm New Password'}
                        autoCorrect={false}
                        // ref={(ref) => this._renewPasswordEntry = ref}
                        maxLength={16}
                        minLength={6}
                        secureTextEntry
                        onChangeText={renewPassword => this.setState({ renewPassword }, () => this.checkrePassword())}
                        style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                </View>
                <Text style={{ color: 'red', left: 15 }}>{disable ? "Password doesnot match" : ''}</Text>
                <View style={Styles.saveAndContinue,{width:300}}>
                    <TouchableOpacity disabled={disable} style={isbutton ? Styles.submitButton1 : Styles.submitButton} onPress={() => { this.save() }}>
                        <Text style={Styles.submitButtonText}>{'Change Password'}</Text>
                    </TouchableOpacity>

                </View>
            </View>
        );
    }

}

// PasswordScreen.navigationOptions = ({ navigation }) => ({
//     headerStyle: Theme.headerStyle,
//     headerLeft: <BackButton navigation={navigation} />,
//     headerTitleStyle: Theme.headerTitleStyle,
//     // title: 'Change Password',
//     headerRight: (<View></View>)

// })

export default PasswordScreen

const Styles = StyleSheet.create({
    saveAndContinue: {
        position: 'absolute',
        bottom: 450,
        left: 20,
        width: width - 40
    },
    submitButton: {
        borderRadius: 4,
        backgroundColor: Theme.primaryColor,
        height: isIphoneX() ? 80 : 50,
        justifyContent: isIphoneX() ? 'flex-start' : 'center',
        alignItems: 'center',
        paddingTop: isIphoneX() ? 20 : 0

    },
    submitButton1: {
        borderRadius: 4,
        backgroundColor: Theme.primaryColor,
        height: isIphoneX() ? 80 : 50,
        justifyContent: isIphoneX() ? 'flex-start' : 'center',
        alignItems: 'center',
        paddingTop: isIphoneX() ? 20 : 0,
        opacity: 0.2
    },
    submitButtonText: {
        fontSize: 16,
        fontFamily: Theme.mediumFont,
        fontWeight: '600',
        lineHeight: 19,
        letterSpacing: 0.5,
        color: Theme.white
    },
    container: {
        flex: 1,
        backgroundColor: Theme.backgroundColor,
        padding: 20,
    },
    title: {
        fontFamily: Theme.mediumFont,
        fontSize: 28,
        fontWeight: '800',
        alignItems: 'center',
    }

})