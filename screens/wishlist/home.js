import React, { Component } from "react";
import {
    View,
    ScrollView,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
    FlatList
} from "react-native";

import Theme from '../../theme/style'
import BackButton from '../../theme/back'
import Styles from './style'
// import { connect } from 'react-redux';
// import { bindActionCreators, compose } from 'redux';
// import * as WhislistAction from '../../redux/actions/whislist'
// import * as CartAction from '../../redux/actions/cart'
// import * as storage from '../../redux/actions/storage'
// import config from '../../config'
// import axios from 'axios';
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

class Whislist extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userLoginData: '',
            wdata: ''
        }
    }

    componentDidMount() {
        this.getWishlist();
    }


    async getWishlist() {
        const { navigation } = this.props;
        await storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
            } else {
                navigation.navigate('LoginScreen', { backTo: 'WhislistScreen', callHome: this.getWishlist.bind(this) })
                // console.log("error", error)
            }
        });
        const { userLoginData, wdata } = this.state;
        var data = {
            appsecret: appsecret,
            userid: userLoginData.id,
            action: 'getwishlist'
        }
        axios.post(BASE_URL + 'wishlist.php', data).then(result => {
            console.log("whistlist",result.data)
            this.setState({ wdata: result.data.data });
        }), err => {
            console.log("err", err)
        }
    }
    render() {

        const { navigation } = this.props;
        const { wdata, userLoginData } = this.state;

        return (
            <View style={Styles.container,{width:200}}>
                <View style={Styles.cartHeader}>
                    <View style={Styles.cartHeaderLeft}>
                        <View style={Styles.cartStatusBottom}>
                            <Text style={Styles.cartStatusText2}>
                                {wdata ? wdata.length + ' items' : ''}
                            </Text>
                        </View>
                    </View>
                </View>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <FlatList
                        data={wdata}
                        renderItem={
                            ({ item }) => {
                                return (
                                    <View style={ProductStyles.item} key={item.product_id}>
                                        <TouchableOpacity onPress={() => navigation.navigate("ProductScreenInProfile", { item })} underlayColor="white">
                                            <Image style={ProductStyles.image} source={{ uri: item.image[0].url }} resizeMode={'cover'} />
                                        </TouchableOpacity>
                                        <View style={ProductStyles.inner}>
                                            <TouchableOpacity onPress={() => navigation.navigate("ProductScreenInProfile", { item })} underlayColor="white">
                                                <Text numberOfLines={5} style={ProductStyles.name}>{item.product_title}</Text>
                                            </TouchableOpacity>
                                            {item.rate != '' && item.discount_price != null ?
                                                <View style={ProductStyles.prices}>
                                                    <Text style={ProductStyles.price_discounted}>${item.rate}
                                                    </Text>
                                                    <Text style={ProductStyles.productPrice}>${item.discount_price}</Text>
                                                </View>
                                                : <View style={ProductStyles.prices}><Text style={ProductStyles.productPrice}>${item.rate}</Text></View>
                                            }
                                            <TouchableOpacity style={ProductStyles.editButton} onPress={() => { this.Deletewishlist(item) }}>
                                                <Image style={{ width: 16, height: 16 }} resizeMode={'contain'}
                                                    source={require('../../assets/images/ic_close.png')} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={ProductStyles.addToCart} onPress={() => { this.addToCartPress(item) }} underlayColor="white">
                                                <Text style={ProductStyles.buttonText}>{'ADD TO CART'}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            }}
                    />
                </ScrollView>

            </View>
        );
    }

    addToCartPress(product) {
        // var attrs = []
        // if (product.attributes) {
        //     product.attributes.map((a) => {
        //         if (a.selected) {
        //             attrs.push(a.name + ": " + a.selected)
        //         }
        //     })
        // }
        // this.props.CartAction.addToCart(product, attrs, 1);
    }
    Deletewishlist(item) {
        var data = {
            action: 'deletewishlist',
            userid: item.user_id,
            product_id: item.product_id,
            appsecret: appsecret
        }
        axios.post(BASE_URL + 'wishlist.php', data).then(result => {
            console.log("result", result.data.status)
            if (result.data.status == "success") {
                this.getWishlist();
            }
            else {
                console.log("error", result.data.status)
            }
        })
    }
}



// Whislist.navigationOptions = ({ navigation }) => ({
//     headerStyle: Theme.headerStyle,
//     headerLeft: <BackButton navigation={navigation} />,
//     headerTitleStyle: Theme.headerTitleStyle,
//     title: 'WHIS LIST',
//     headerRight: (<View></View>)
// })


// function mapStateToProps(state) {
//     return {
//         whislist: state.whislist
//     };
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         WhislistAction: bindActionCreators(WhislistAction, dispatch),
//         CartAction: bindActionCreators(CartAction, dispatch),
//     };
// }

export default (Whislist);


const ProductStyles = StyleSheet.create({
    image: {
        width: 96,
        height: 124,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#E6E8EC'
    },
    item: {
        padding: 15,
        paddingBottom: 0,
        flexDirection: 'row',
    },
    inner: {
        padding: 15,
        paddingBottom: 0,
        paddingTop: 8,
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'space-between'
    },
    // prices: {
    //     flexDirection: 'row'
    // },
    price_discounted: {
        textAlign: 'left',
        fontFamily: Theme.boldFont,
        fontSize: 13,
        lineHeight: 24,
        color: Theme.primaryColor,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        opacity: 0.75,
        fontWeight: '500',
        letterSpacing: 0.5
    },
    prices: {
        flexDirection: 'column',
        flex: 0.5,
        fontSize: 15,
        justifyContent: 'flex-start'
    },
    name: {
        width: '100%',
        fontFamily: Theme.mediumFont,
        fontSize: 14,
        letterSpacing: 1,
        marginBottom: 10,
        color: Theme.primaryColor,
        fontWeight: '500'
    },
    addToCart: {
        backgroundColor: Theme.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        height: 32,
        width: 140,
        borderRadius: 4
    },
    editButton: {
        position: 'absolute',
        right: 15,
        top: 20
    },
    buttonText: {
        fontFamily: Theme.mediumFont,
        fontSize: 14,
        letterSpacing: 0.5,
        textAlign: 'center',
        fontWeight: '600',
        color: 'white'
    },


})