import React, { Component, PropTypes } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView, FlatList
} from "react-native";
// import NetInfo from "@react-native-community/netinfo";
import styles from '../partials/style'
import Theme from '../../../theme/style'
import MenuButton from '../../../theme/menuButton'
// import { Dropdown } from 'react-native-material-dropdown';
import EmptyCartHolder from './empty'
import Loading from '../../../common/loading'
// import CartButton from '../../../common/CartButton'
import Styles from './style'
// import ProductListItem from '../partials/product-list-item'
import FormStyle from '../../../theme/form'
// import * as CartAction from '../../../redux/actions/cart'
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import * as storage from "../../../redux/actions/storage";
// import * as PaymentAction from '../../../redux/actions/payment'
// import * as ShippingMethodsAction from '../../../redux/actions/shipping-methods'
// import config from '../../../config'
// import axios from 'axios';
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`


class CartScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userLoginData: '',
            cartdata: '',
            totalqty: 0,
            totalamt: 0,
            status: '',
            connection_Status: '',
            isloading: false
        }
        // this.data = "";
        // storage.storageGet("user").then((result) => {
        //     if (result != null) {
        //         this.setState({ userLoginData: JSON.parse(result) })
        //         this.getCartOrder();
        //     } else {
        //         console.log("error")
        //     }
        // });

    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.state.value) {
            this.setState({ value: nextProps.value });
            this.getCartOrder();
            console.log("erorr123456", nextProps.value)
        }
    }
    // componentDidMount() {
    //     this.props.navigation.addListener('willFocus', (route) => {
    //         this.getCartOrder();
    //     });
    //     NetInfo.addEventListener(state1 => {
    //         console.log('Connection type', state1.type);
    //         this.setState({ connection_Status: state1 });
    //         if (state1.isConnected) {
    //             this.setState({ context: "You are online" });
    //         }
    //         else {
    //             this.setState({ context: "You are offline" });
    //         }
    //     })
    // }
    // componentDidUpdate() {
    //     if (this.state.connection_Status.isConnected)
    //         setTimeout(() => this.setState({ context: '' }), 3000);
    // }

    // async getCartOrder() {
    //     this.setState({ isloading: true });
    //     await storage.storageGet("user").then((result) => {
    //         if (result != null) {
    //             this.setState({ userLoginData: JSON.parse(result) })
    //         } else {
    //             console.log("error")
    //         }
    //     });
    //     const { userLoginData } = this.state
    //     var data = {
    //         userid: userLoginData.id,
    //         action: 'getcart',
    //         appsecret: appsecret
    //     }
    //     var data1 = {
    //         action: "getcartcount",
    //         appsecret: appsecret,
    //         userid: userLoginData.id,
    //     }
    //     axios.post(BASE_URL + 'cart.php', data1).then(result => {
    //         this.setState({ isloading: false });
    //         this.setState({ totalqty: result.data.data });
    //         this.setState({ totalamt: result.data.total });
    //     }),
    //         err => {
    //             this.setState({ isloading: false });
    //             console.log("error", err);
    //         }
    //     axios.post(BASE_URL + 'cart.php', data).then(result => {
    //         this.setState({ isloading: false });
    //         if (result.data.status == "success") {
    //             if (result.data.data)
    //                 this.setState({ cartdata: result.data.data })
    //             else {
    //                 this.setState({ isloading: false });
    //                 this.setState({ cartdata: '' })
    //                 console.log("no data")
    //             }
    //         }
    //         else {
    //             this.setState({ isloading: false });
    //             this.setState({ cartdata: '' })
    //             this.setState({ totalqty: 0 });
    //             this.setState({ totalamt: 0 });
    //         }
    //     }),
    //         err => {
    //             this.setState({ isloading: false });
    //             console.log("error", err)
    //         }
    // }

    // nav(route, total) {
    //     const customer = this.state.userLoginData
    //     const { navigation } = this.props
    //     if (!customer) {
    //         navigation.navigate('LoginScreen', { backTo: 'CheckoutAddress' })
    //     } else {
    //         navigation.navigate(route, { total })
    //     }
    // }

    Deleteproduct(item) {
        const { isloading } = this.state;
        this.setState({ isloading: true })
        var data = {
            action: 'deletecart',
            userid: item.user_id,
            p_id: item.product_id,
            appsecret: appsecret
        }
        console.log(item)
        axios.post(BASE_URL + 'cart.php', data).then(result => {
            this.setState({ isloading: false })
            this.getCartOrder();
            CartButton()
            {
                CartButton.getqty();
            };
        })
    }
    quantityChange(item, value) {
        this.setState({ isloading: true })
        var data = {
            action: 'addcart',
            userid: item.user_id,
            p_id: item.product_id,
            qty: value,
            appsecret: appsecret
        }
        axios.post(BASE_URL + 'cart.php', data).then(result => {
            this.setState({ isloading: false })
            console.log("error", result.data.data)
            if (result.data.data) {
                this.getCartOrder();
                CartButton()
                {
                    CartButton.getqty();
                };
            }
        })

    }

    render() {

        const { navigation } = this.props
        const { cartdata, totalamt, totalqty, isloading } = this.state;
        if (!cartdata) {
            return (
                <EmptyCartHolder navigation={navigation} />
            )
        }
        else {
            return (
                <View style={Styles.cartContainer}>
                    <View style={Styles.cartHeader}>
                        <View style={Styles.cartHeaderLeft}>
                            <View>
                                <Text style={Styles.cartStatusText}>
                                    {'YOUR SHOPPING CART'}
                                </Text>
                            </View>
                            <View style={Styles.cartStatusBottom}>
                                <Text style={Styles.cartStatusText2}>
                                    {'Review'} {totalqty}{' items'}
                                </Text>
                                <Text style={Styles.cartStatusText3}>
                                    {' $' + totalamt}
                                </Text>
                            </View>

                        </View>
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>
                        <View style={Styles.cartProducts}>
                            <View style={{ flex: 1.0 }}>
                                <FlatList
                                    data={cartdata}
                                    keyExtractor={(item) => item.id}
                                    renderItem={({ item }) => {
                                        return (
                                            <View style={styles.productListItem}>
                                                <TouchableOpacity style={[styles.imageContainer]}>
                                                    <Image resizeMode={'cover'}
                                                        source={{ uri: item.image }} style={styles.image} />
                                                </TouchableOpacity>

                                                <View style={styles.content}>
                                                    <TouchableOpacity style={styles.nameContainer} onPress={() => { }}>
                                                        <Text style={styles.name}>{item.product_title}</Text>
                                                    </TouchableOpacity>
                                                    <View style={styles.priceContainer}>

                                                        {item.rate > 0 && < Text style={item.discount_price > 0 ? styles.discount : styles.price}>${item.rate}</Text>}
                                                        {item.discount_price > 0 && <Text style={styles.price}>${item.discount_price}</Text>}
                                                    </View>
                                                </View>
                                                {isloading && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                                    <Loading />
                                                </View>}
                                                <TouchableOpacity style={styles.editButton} onPress={() => { this.Deleteproduct(item) }}>
                                                    <Image style={{ width: 16, height: 16 }} resizeMode={'contain'}
                                                        source={require('../../../assets/images/ic_close.png')} />
                                                </TouchableOpacity>

                                                <View style={styles.Dropdown}>
                                                    {/* <Dropdown
                                                        renderAccessory={() =>
                                                            <Image
                                                                resizeMode={'contain'}
                                                                style={{ opacity: 0.6, position: 'relative', top: 7, marginLeft: 7 }}
                                                                source={require('../../../assets/images/ic_down.png')} />
                                                        }
                                                        fontSize={14}
                                                        itemPadding={5}
                                                        baseColor={'transparent'}
                                                        onChangeText={(value) => { this.quantityChange(item, value) }}
                                                        itemTextStyle={{ fontFamily: Theme.regularFont, }}
                                                        label={'Quantity'}
                                                        data={[
                                                            { name: '01', value: 1 },
                                                            { name: '02', value: 2 },
                                                            { name: '03', value: 3 },
                                                            { name: '04', value: 4 },
                                                            { name: '05', value: 5 },
                                                            { name: '06', value: 6 },
                                                            { name: '07', value: 7 },
                                                            { name: '08', value: 8 },
                                                            { name: '09', value: 9 },
                                                            { name: '10', value: 10 },
                                                        ]}
                                                        value={item.quantity}
                                                    /> */}
                                                </View>

                                            </View >
                                        )
                                    }}
                                />
                            </View>

                        </View>

                        <View style={FormStyle.pageDoneWithDiscard}>
                            <View style={FormStyle.pageDiscardView}>
                                <TouchableOpacity style={FormStyle.pageDiscardButton} onPress={() => { this.props.navigation.navigate('HomeScreen') }}>
                                    <Image source={require('../../../assets/images/ic_arrow_left.png')} style={FormStyle.pageDiscardButtonImage} resizeMode={'contain'} />
                                    <Text style={FormStyle.pageDiscardButtonText}>Continue Shopping</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={FormStyle.pageDoneView}>
                                <TouchableOpacity style={FormStyle.pageDoneButton} onPress={() => { this.nav('CheckoutAddress', totalamt) }}>
                                    <Text style={FormStyle.pageDoneButtonText}>CHECK OUT</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </View>

            )
        }
    }
}

// CartScreen.navigationOptions = ({ navigation }) => ({
//     title: 'CART',
//     headerLeft: <MenuButton navigation={navigation} />,
//     headerStyle: Theme.headerStyle,
//     headerTitleStyle: Theme.headerTitleStyle,
//     headerRight: (<View />)
// })
export default (CartScreen);