import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity, FlatList
} from "react-native";
import Theme from '../../../theme/style'
import styles from './style'
import { Dropdown } from 'react-native-material-dropdown';
import * as CartAction from '../../../redux/actions/cart'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import config from '../../../config'
import axios from 'axios';
import * as storage from "../../../redux/actions/storage";
export const BASE_URL = `${config.baseurl}/`
export const appsecret = `${config.appsecret}`
class ProductListItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userLoginData: '',
            cartdata: ''
        }

        this.getCartOrder();
    }


    async getCartOrder() {
        await storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
            } else {
                console.log("error")
            }
        });
        const { userLoginData } = this.state
        var data = {
            userid: userLoginData.id,
            action: 'getcart',
            appsecret: appsecret
        }
        axios.post(BASE_URL + 'cart.php', data).then(result => {
            this.setState({ cartdata: result.data.data })
        }),
            err => {
                console.log("error", err)
            }
    }

    componentDidMount() {
        this.getCartOrder();

    }

    render() {
        const { cartdata } = this.state;
        return (
            s
        )
    }
}
function mapStateToProps(state) {
    return {
        cart: state.cart,
    };
}
function mapDispatchToProps(dispatch) {
    return {
        CartAction: bindActionCreators(CartAction, dispatch),
    };
}



export default connect(mapStateToProps, mapDispatchToProps)(ProductListItem);