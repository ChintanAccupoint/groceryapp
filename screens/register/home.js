import React, { Component } from 'react'
import { View, Text, Keyboard, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native'
import Theme from '../../theme/style'
import FormStyle from '../../theme/form'
import PageStyle from './style'
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import { DropDownHolder } from '../../common/dropalert'
// import Loading from '../../common/loading'

class RegisterScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            inputFocused: false,
            firstName: undefined,
            lastName: undefined,
            Email: undefined,
            Password: undefined,
            Mobile: undefined,
            loading: false
        }

        this._firstNameEntry = undefined;
        this._lastNameEntry = undefined;
        this._EmailEntry = undefined;
        this._PasswordEntry = undefined;
        this._MobileEntry = undefined;
        this._PasswordEntry = undefined;
        this.keyboardBehavior = "padding";
    }

    componenDidMount() {
        this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
        this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
        this.keyboardWillHide = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)
        this.keyboardDidHide = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)
    }


    showLoading() {
        this.setState({ loading: true })
    }

    hideLoading() {
        this.setState({ loading: false })
    }

    signUp() {
        const { firstName, lastName, Email, Mobile, Password } = this.state
        const formIsValid =
            this.validateAndSetAttribute(firstName, this._firstNameEntry) &
            this.validateAndSetAttribute(lastName, this._lastNameEntry) &
            this.validateAndSetAttribute(Email, this._EmailEntry) &
            this.validateAndSetAttribute(Mobile, this._MobileEntry) &
            this.validateAndSetAttribute(Password, this._PasswordEntry);

        if (formIsValid === 1) {
            console.log("enterd in",this.state)

            auth()
            .createUserWithEmailAndPassword(this.state.Email,this.state.Password)
            .then(() => {
              console.log('User account created & signed in!');
                    firestore()
                    .collection('users')
                    .add({
                        firstName : this.state.firstName,
                        lastName:this.state.lastName,
                        Email : this.state.Email,
                        Mobile : this.state.Mobile,
                        Password:this.state.Password
                    })
                    .then(() => {
                        console.log('User added!');
                        this.props.navigation.navigate('LoginScreen')
                    });
            })
            .catch(error => {
              if (error.code === 'auth/email-already-in-use') {
                alert('That email address is already in use!');
              }
          
              if (error.code === 'auth/invalid-email') {
                alert('That email address is invalid!');
              }
          
              console.error(error);
            });

        } else {
            alert('Please enter your login informations.')
        }
    }

    validateInput(input) {
        if (input === undefined)
            return false
        else if (input === '')
            return false
        else if (input.trim() === '')
            return false
        else if (input === 0)
            return false
        else
            return true
    }

    validateAndSetAttribute(value, attribute) {
        const valid = this.validateInput(value)
        const borderBottomColor = !valid ? 'red' : '#E8E8E8';
        attribute.setNativeProps({
            style: { borderBottomColor }
        });
        return valid
    }

    render() {
        const { loading, inputFocused } = this.state
        return (

            <KeyboardAvoidingView behavior={this.keyboardBehavior} style={[FormStyle.container, PageStyle.container2]}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} alwaysBounceVertical={true}>
                <View >
                        <View style={PageStyle.PageTitleView}>
                            <Image source={require('../../assets/images/SplashImage.png')}
                                resizeMode={'contain'}
                                style={PageStyle.Logo} />
                        </View>

                        <View style={[FormStyle.form, { paddingHorizontal: 10, paddingTop: 6 }]}>

                            <View style={FormStyle.row}>
                                { /* First Name Input Form Group */}
                                <View style={FormStyle.col2}>
                                    <View style={[FormStyle.formGroup, FormStyle.formGrouplogin]}>
                                        <TextInput placeholderTextColor={Theme.secondaryColor}
                                            placeholder={'First Name'}
                                            autoCorrect={false}
                                            ref={(firstName) => this._firstNameEntry = firstName}
                                            onChangeText={(firstName) => this.setState({ firstName })}
                                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                                    </View>
                                </View>
                                { /* Last Name Input Form Group */}
                                <View style={FormStyle.col2}>
                                    <View style={[FormStyle.formGroup, FormStyle.formGrouplogin]}>
                                        <TextInput placeholderTextColor={Theme.secondaryColor}
                                            placeholder={'Last Name'}
                                            autoCorrect={false}
                                            ref={(lastName) => this._lastNameEntry = lastName}
                                            onChangeText={(lastName) => this.setState({ lastName })}
                                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                                    </View>
                                </View>
                            </View>

                            { /* Email Input Form Group */}
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Mobile No'}
                                    autoCorrect={false}
                                    keyboardType='numeric'
                                    maxLength={10}
                                    ref={(Mobile) => this._MobileEntry = Mobile}
                                    onChangeText={(Mobile) => this.setState({ Mobile })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>

                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Email'}
                                    autoCorrect={false}
                                    ref={(Email) => this._EmailEntry = Email}
                                    onChangeText={(Email) => this.setState({ Email })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>

                            { /* Password Input Form Group */}
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput secureTextEntry={true}
                                    placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Password'}
                                    autoCorrect={false}
                                    ref={(Password) => this._PasswordEntry = Password}
                                    onChangeText={(Password) => this.setState({ Password })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>

                        </View>

                        {inputFocused === false &&
                            <TouchableOpacity style={[FormStyle.primaryButton, { marginBottom: 0, marginTop: 70 }]}
                            onPress={() => { this.signUp() }} >
                                <Text style={FormStyle.primaryButtonText}>{'SIGN UP'}</Text>
                            </TouchableOpacity>}
                        {loading && <View style={PageStyle.loadingWrap}>
                            <Loading />
                        </View>}
                    </View>
                </ScrollView>


             
                    <View style={[PageStyle.otherOptions, { marginBottom: 20 }]}>
                        { /* Sign In Button */}
                        <TouchableOpacity style={FormStyle.secondaryButton} onPress={() => this.props.navigation.navigate('LoginScreen')}  >
                            <Text style={FormStyle.secondaryButtonText}>{'Already a member?'}</Text>
                            <Text style={FormStyle.secondaryButtonPrimaryText}>SIGN IN</Text>
                        </TouchableOpacity>
                    </View>
                
            </KeyboardAvoidingView>



        )
    }

    keyboardDidShow = () => {
        this.setState({ inputFocused: true })
    }
    keyboardWillShow = () => {
        this.setState({ inputFocused: true })
    }
    keyboardWillHide = () => {
        this.setState({ inputFocused: false })
    }
    keyboardDidHide = () => {
        this.setState({ inputFocused: false })
    }

}

export default (RegisterScreen);
