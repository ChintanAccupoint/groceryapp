import React, { Component, PropTypes } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    TextInput,
    KeyboardAvoidingView,
    Keyboard,
    Image,
    Platform
} from "react-native";

import Theme from '../../theme/style'
import BackButton from '../../theme/back'
import Styles from './style'
import FormStyle from '../../theme/form'
// import ModalFilterPicker from '../../components/ModalFilterPicker/index'
// import axios from 'axios';
// import config from '../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`
// import * as storage from "../../redux/actions/storage";
// import location from '../../common/location/location'
import { DropDownHolder } from '../../common/dropalert'

// const countries = location.getCountries()

class Address extends Component {

    constructor(props) {
        super(props)

        // this.state = {
        //     inputFocused: false,
        //     first_name: undefined,
        //     last_name: undefined,
        //     city: undefined,
        //     postcode: undefined,
        //     address_1: undefined,
        //     address_2: undefined,
        //     state: undefined,
        //     country: undefined,
        //     email: undefined,
        //     title: undefined,
        //     phone: undefined,
        //     citySelectVisible: false,
        //     countrySelectVisible: false,
        //     cityOptions: [],
        //     countryOptions: [],
        //     selectedCityName: 'State',
        //     selectedCountryName: 'Country',
        //     selectedShippingMethod: { id: 0 },
        //     userLoginData: '',
        //     addressid: ''
        // }

        // this._first_nameEntry = undefined;
        // this._last_nameEntry = undefined;
        // this._cityEntry = undefined;
        // this._postcodeEntry = undefined;
        // this._address1Entry = undefined;
        // this._address2Entry = undefined;
        // this._stateEntry = undefined;
        // this._countryEntry = undefined;
        // this._titleEntry = undefined;
        // this._emailEntry = undefined;
        // this._phoneEntry = undefined;
        this.keyboardBehavior = "padding";
    }

    // componentDidMount() {

    //     this.getaddress()

    //     if (Platform.OS == 'android') {
    //         this.keyboardBehavior = 'height'
    //     }

    //     this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    //     this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
    //     this.keyboardWillHide = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)
    //     this.keyboardDidHide = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)

    //     let keyValuePair = []
    //     for (var i = 0; i < countries.length; i++) {
    //         keyValuePair.push({
    //             key: countries[i].filename || countries[i].name,
    //             label: countries[i].name
    //         })
    //     }
    //     this.setState({
    //         countryOptions: keyValuePair
    //     })
    // }

    submit() {
        // const { currentCustomer } = this.props
        // const { first_name, userLoginData, addressid, last_name, title, selectedCityName, selectedCountryName, postcode, address_1, address_2, state, email, phone } = this.state
        // const formIsValid =
        //     this.validateAndSetAttribute(first_name, this._first_nameEntry) &
        //     this.validateAndSetAttribute(last_name, this._last_nameEntry) &
        //     // this.validateAndSetAttribute(selectedCountryName === 'Country' ? '' : selectedCountryName, this._countryEntry) &
        //     this.validateAndSetAttribute(selectedCityName, this._cityEntry) &
        //     this.validateAndSetAttribute(title, this._titleEntry) &
        //     this.validateAndSetAttribute(postcode, this._postcodeEntry) &
        //     this.validateAndSetAttribute(address_1, this._address1Entry) &
        //     this.validateAndSetAttribute(email, this._emailEntry) &
        //     this.validateAndSetAttribute(phone, this._phoneEntry) &
        //     this.validateAndSetAttribute(state, this._stateEntry);

        // if (formIsValid === 1) {


        //     var address = {
        //         action: 'addaddress',
        //         first_name: first_name,
        //         address_1: address_1,
        //         address_2: address_2 || '',
        //         state: selectedCityName == 'City' ? selectedCountryName : selectedCityName,
        //         city: state,
        //         postcode: postcode,
        //         country: selectedCountryName,
        //         email: email,
        //         title: title,
        //         phone: phone || '',
        //         id: addressid,
        //         appsecret: appsecret,
        //         userid: userLoginData.id
        //     }
        //     var shipping = {
        //         id: addressid,
        //         first_name: first_name,
        //         last_name: last_name,
        //         company: "",
        //         address_1: address_1,
        //         address_2: address_2 || '',
        //         state: selectedCityName == 'City' ? selectedCountryName : selectedCityName,
        //         city: state,
        //         postcode: postcode,
        //         country: selectedCountryName,
        //         appsecret: appsecret
        //     }
        //     axios.post(BASE_URL + 'address.php', address).then(result => {
        //         if (result.data.status == "update_success") {
        //             DropDownHolder.alert('success', 'updated', 'Your address has been updated successfully.')
        //         }
        //         else if (result.data.status == "success") {
        //             DropDownHolder.alert('success', 'Successfull', 'Your address has been saved successfully.')
        //         }
        //         else {
        //             DropDownHolder.alert('error', '', 'something went wrong')
        //         }
        //     }),
        //         err => {
        //             // DropDownHolder.alert('error', '', 'something went wrong')
        //             console.log("error")
        //         }
        // } else {
        //     DropDownHolder.alert('error', '', 'Please enter your address details.')
        // }
    }

    // async getaddress() {
    //     await storage.storageGet("user").then((result) => {
    //         if (result != null) {
    //             this.setState({ userLoginData: JSON.parse(result) })
    //         } else {
    //             console.log("error")
    //         }
    //     });
    //     const { userLoginData } = this.state;
    //     var data = {
    //         userid: userLoginData.id,
    //         action: 'getaddress',
    //         appsecret: appsecret
    //     }
    //     axios.post(BASE_URL + 'address.php', data).then(result => {
    //         if (result.data.data) {
    //             var data = result.data.data;
    //             this.setState({ first_name: data.first_name })
    //             this.setState({ last_name: data.last_name })
    //             this.setState({ title: data.title })
    //             this.setState({ address_1: data.address1 })
    //             this.setState({ address_2: data.address2 })
    //             this.setState({ selectedCountryName: data.country })
    //             this.setState({ selectedCityName: data.state })
    //             this.setState({ state: data.city })
    //             this.setState({ city: data.state })
    //             this.setState({ postcode: data.pin_code })
    //             this.setState({ phone: data.mobileno })
    //             this.setState({ first_name: data.first_name })
    //             this.setState({ last_name: data.last_name })
    //             this.setState({ email: data.email })
    //             this.setState({ addressid: data.id })
    //         }
    //     }),
    //         err => {
    //             console.log("error", err)
    //         }
    // }


    render() {
        // const { citySelectVisible, address_1, countrySelectVisible, cityOptions, countryOptions, selectedCountryName, selectedCityName, selectedShippingMethod } = this.state

        return (
            <KeyboardAvoidingView behavior={this.keyboardBehavior} style={Styles.container,{width:350,marginLeft:30}}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} alwaysBounceVertical={true}>
                    {/* <View style={Styles.pageHeading}>
                        <Text style={Styles.pageHeadingText}>{'Your Delivery Address'}</Text>
                    </View> */}
                    <View style={FormStyle.row}>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'First Name'}
                                    autoCorrect={false}
                                    maxLength={60}
                                    editable={false}
                                    onChangeText={first_name => this.setState({ first_name })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]}
                                    // value={this.state.first_name}
                                    // ref={(ref) => this._first_nameEntry = ref}
                                />
                            </View>
                        </View>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Last Name'}
                                    autoCorrect={false}
                                    // ref={(ref) => this._last_nameEntry = ref}
                                    maxLength={60}
                                    editable={false}
                                    onChangeText={last_name => this.setState({ last_name })}
                                    // value={this.state.last_name}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>

                    </View>
                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        <TextInput placeholderTextColor={Theme.secondaryColor}
                            placeholder={'Title'}
                            autoCorrect={false}
                            // ref={(ref) => this._titleEntry = ref}
                            maxLength={160}
                            onChangeText={title => this.setState({ title })}
                            // value={this.state.title}
                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                    </View>
                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        <TextInput placeholderTextColor={Theme.secondaryColor}
                            placeholder={'Address Line 1'}
                            autoCorrect={false}
                            // ref={(ref) => this._address1Entry = ref}
                            maxLength={160}
                            onChangeText={address_1 => this.setState({ address_1 })}
                            // value={this.state.address_1}
                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                    </View>
                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        <TextInput
                            placeholderTextColor={Theme.secondaryColor}
                            placeholder={'Address Line 2'}
                            autoCorrect={false}
                            // ref={(ref) => this._address2Entry = ref}
                            maxLength={160}
                            onChangeText={address_2 => this.setState({ address_2 })}
                            // value={this.state.address_2}
                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                    </View>
                    <View style={FormStyle.row}>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TouchableOpacity
                                    style={FormStyle.dropdownButton}
                                    // ref={(ref) => this._countryEntry = ref}
                                    onPress={() => { this.countryOnShow() }}>
                                    <Text style={[FormStyle.dropdownButtonText]}></Text>
                                    <Image style={FormStyle.dropdownButtonImage} source={require('../../assets/images/ic_down.png')} resizeMode={'cover'} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TouchableOpacity
                                    style={FormStyle.dropdownButton}
                                    // ref={(ref) => this._cityEntry = ref}
                                    onPress={() => { this.cityOnShow() }}>
                                    <Text style={[FormStyle.dropdownButtonText]}></Text>
                                    <Image style={FormStyle.dropdownButtonImage} source={require('../../assets/images/ic_down.png')} resizeMode={'cover'} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={FormStyle.row}>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'City'}
                                    autoCorrect={false}
                                    // ref={(ref) => this._stateEntry = ref}
                                    maxLength={60}
                                    // onChangeText={state => this.setState({ state })}
                                    // value={this.state.state}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Zip Code'}
                                    autoCorrect={false}
                                    // ref={(ref) => this._postcodeEntry = ref}
                                    maxLength={6}
                                    // onChangeText={postcode => this.setState({ postcode })}
                                    // value={this.state.postcode}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>
                    </View>
                    <View style={FormStyle.row}>
                        <View style={FormStyle.col1}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Email'}
                                    autoCorrect={false}
                                    // ref={(ref) => this._emailEntry = ref}
                                    maxLength={200}
                                    editable={false}
                                    keyboardType={'email-address'}
                                    onChangeText={email => this.setState({ email })}
                                    // value={this.state.email}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>
                    </View>
                    <View style={FormStyle.row}>
                        <View style={FormStyle.col1}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Phone'}
                                    autoCorrect={false}
                                    keyboardType={'phone-pad'}
                                    // ref={(ref) => this._phoneEntry = ref}
                                    maxLength={200}
                                    onChangeText={phone => this.setState({ phone })}
                                    // value={this.state.phone}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>
                    </View>
                </ScrollView>

                <View style={[ { paddingTop:15 } ]}>
                    <TouchableOpacity style={FormStyle.submitButton} onPress={() => { this.submit() }}>
                        <Text style={FormStyle.submitButtonText}>{'SAVE CHANGES'}</Text>
                    </TouchableOpacity>
                </View>

                {/* <ModalFilterPicker
                    visible={countrySelectVisible}
                    onSelect={(v) => { this.countryOnSelect(v) }}
                    onCancel={() => { this.countryOnCancel() }}
                    options={countryOptions}
                    placeholderText={'Search Country...'}
                    placeholderTextColor={Theme.secondaryColor}
                    noResultsText={'No matches.'}
                    cancelButtonText={'CLOSE'}
                    cancelButtonStyle={{ backgroundColor: 'transparent', marginTop: 20 }}
                    cancelButtonTextStyle={{ fontFamily: Theme.regularFont, fontSize: 17, color: Theme.white, fontWeight: '500' }}
                    optionTextStyle={{
                        padding: 8,
                        flex: 1,
                        textAlign: 'left',
                        fontFamily: Theme.regularFont,
                        fontSize: 17,
                        color: Theme.primaryColor,
                        fontWeight: '500'
                    }}
                /> */}
                {/* <ModalFilterPicker
                    visible={citySelectVisible}
                    onSelect={(v) => { this.cityOnSelect(v) }}
                    onCancel={() => { this.cityOnCancel() }}
                    options={cityOptions}
                    placeholderText={'Search state...'}
                    placeholderTextColor={Theme.secondaryColor}
                    noResultsText={'No matches.'}
                    cancelButtonText={'CLOSE'}
                    cancelButtonStyle={{ backgroundColor: 'transparent', marginTop: 20 }}
                    cancelButtonTextStyle={{ fontFamily: Theme.regularFont, fontSize: 17, color: Theme.white, fontWeight: '500' }}
                    optionTextStyle={{
                        padding: 8,
                        flex: 1,
                        textAlign: 'left',
                        fontFamily: Theme.regularFont,
                        fontSize: 17,
                        color: Theme.primaryColor,
                        fontWeight: '500'
                    }}
                /> */}
            </KeyboardAvoidingView>
        );
    }

    validateInput(input) {
        if (input === undefined)
            return false
        else if (input === '')
            return false
        else if (input.trim() === '')
            return false
        else if (input === 0)
            return false
        else
            return true
    }
    validateAndSetAttribute(value, attribute) {
        const valid = this.validateInput(value)
        const borderBottomColor = !valid ? 'red' : '#E8E8E8';
        attribute.setNativeProps({
            style: { borderBottomColor }
        });
        return valid
    }
    // countryOnSelect(key) {
    //     let label = '';
    //     for (var i = 0; i < countries.length; i++) {
    //         if (countries[i].filename == key) {
    //             label = countries[i].name
    //             break;
    //         }
    //         else if (countries[i].name == key) {
    //             label = countries[i].name;
    //             break;
    //         }
    //     }


    //     var cities = location.getCities(key)
    //     let keyValuePair = []
    //     if (cities.length > 0) {
    //         for (var j = 0; j < cities.length; j++) {
    //             keyValuePair.push({
    //                 key: cities[j].name,
    //                 label: cities[j].name
    //             })
    //         }
    //     } else {
    //         this.setState({ selectedCityName: 'City', city: 'City' })
    //     }

    //     this.setState({
    //         country: key,
    //         countrySelectVisible: false,
    //         selectedCountryName: label,
    //         cityOptions: keyValuePair
    //     });
    // }
    // countryOnShow() {
    //     this.setState({
    //         countrySelectVisible: true
    //     });
    // }
    // countryOnCancel = () => {
    //     this.setState({
    //         countrySelectVisible: false,
    //         // country: undefined,
    //         // selectedCountryName: 'Country',
    //         // cityOptions: []
    //     });
    // }
    // cityOnSelect(key) {
    //     this.setState({
    //         city: key,
    //         citySelectVisible: false,
    //         selectedCityName: key
    //     });
    // }
    // cityOnShow() {
    //     this.setState({
    //         citySelectVisible: true
    //     });
    // }
    // cityOnCancel = () => {
    //     this.setState({
    //         citySelectVisible: false,
    //         // city: undefined,
    //         // selectedCityName: 'City'
    //     });
    // }
    keyboardDidShow = () => {
        this.setState({ inputFocused: true })
    }
    keyboardWillShow = () => {
        this.setState({ inputFocused: true })
    }
    keyboardWillHide = () => {
        this.setState({ inputFocused: false })
    }
    keyboardDidHide = () => {
        this.setState({ inputFocused: false })
    }
}

// Address.navigationOptions = ({ navigation }) => ({
//     headerStyle: Theme.headerStyle,
//     headerTitleStyle: Theme.headerTitleStyle,
//     title: 'ADDRESS',
//     headerLeft: <BackButton navigation={navigation} />,
//     headerRight: (<View />)

// })


export default (Address);
