import React, { Component } from "react";
import {
    View,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    Dimensions,
    StyleSheet,
    Image
} from "react-native";

import Theme from '../../theme/style'
import BackButton from '../../theme/back'
import FormStyle from '../../theme/form'
const width = Dimensions.get("window").width
import isIphoneX from '../../common/iphonex'
// import DateTimePickerModal from "react-native-modal-datetime-picker";
// import ImagePicker from 'react-native-image-picker'
// import axios from 'axios';
// import config from '../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`
// import * as storage from "../../redux/actions/storage";
// import { DropDownHolder } from '../../common/dropalert'

class SettingsProfile extends Component {


    // changeAvatar = () => {
    //     const options = {
    //         title: 'select a photo',
    //         takePhotoButtonTitle: 'Take a Photo',
    //         chooseFrmoLibraryButtonTitle: 'Choose from Gallery',
    //         quality: 1
    //     };
    //     ImagePicker.launchImageLibrary(options, response => {
    //         if (response.uri) {
    //             this.setState({ photo: response.uri })
    //             this.setState({ photoobj: response })
    //         }
    //     })
    // }
    validateInput(input) {
        if (input === undefined)
            return false
        else if (input === '')
            return false
        else if (input.trim() === '')
            return false
        else if (input === 0)
            return false
        else
            return true
    }
    validateAndSetAttribute(value, attribute) {
        const valid = this.validateInput(value)
        const borderBottomColor = !valid ? 'red' : '#E8E8E8';
        attribute.setNativeProps({
            style: { borderBottomColor }
        });
        return valid
    }

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (pickeddate) => {
        day = pickeddate.getDate();
        month = pickeddate.getMonth() + 1;
        year = pickeddate.getFullYear();
        this.setState({ dob: day + '-' + month + '-' + year })
        this._hideDateTimePicker();
    };
    constructor(props) {

        super(props)
        this.state = {
            isDateTimePickerVisible: false,
            dob: '',
            photo: null,
            first_name: '',
            last_name: '',
            email: '',
            mobileno: '',
            userLoginData: ''
        };
        this._first_nameEntry = undefined;
        this._last_nameEntry = undefined;
        this._dobEntry = undefined;
        this._emailEntry = undefined;
        this._mobileEntry = undefined;
        this.keyboardBehavior = "padding";

        this.getstorageData();
    }
    async getstorageData() {
        await storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
                this.getdetails();
            } else {
                console.log("error")
            }
        });

    }

    save() {

        const { photo, email,photoobj, first_name, last_name, mobileno, userLoginData, dob } = this.state;
        const formIsValid =
            this.validateAndSetAttribute(first_name, this._first_nameEntry) &
            this.validateAndSetAttribute(last_name, this._last_nameEntry) &
            this.validateAndSetAttribute(email, this._emailEntry) &
            this.validateAndSetAttribute(mobileno, this._phoneEntry);
        this.validateAndSetAttribute(dob, this._dobEntry);
        if (formIsValid === 1) {
            storage.storageRemove('user');
            var data = new FormData();
            var name = "";
            if (photoobj) {
                name = photoobj.uri.split("/").pop();
                data.append('file', {
                    uri: photoobj.uri, // your file path string
                    name: name,
                    type: 'image/jpg'
                })
            }
            data.append('firstname', first_name);
            data.append('email', email);
            data.append('lastname', last_name);
            data.append('mobileno', mobileno);
            data.append('dob', dob);
            data.append('appsecret', appsecret);
            data.append('action', 'updateuser');
            data.append('id', userLoginData.id);
            axios.post(BASE_URL + 'updateuser.php', data,
            ).then(result => { 
                if (result.data.status == "success") {
                    userLoginData.first_name = first_name;
                    userLoginData.last_name = last_name;
                    userLoginData.email = email;
                    userLoginData.dob = dob;
                    if (result.data.data[0].profile_image) {
                        userLoginData.profile_image = result.data.data[0].profile_image;
                    }
                    
                    storage.storageSet('user', JSON.stringify(userLoginData));
                    DropDownHolder.alert('success', 'Successful', 'Your profile has been saved successfully.')
                }
                else {
                    DropDownHolder.alert('error', '', 'something went wrong')
                }
            }),
                err => {
                    // DropDownHolder.alert('error', '', 'something went wrong')
                    console.log("error",err)
                }
        } else {
            DropDownHolder.alert('error', '', 'Please enter your details.')
        }
    }

    getdetails() {
        const { userLoginData } = this.state;
        this.setState({
            first_name: userLoginData.first_name,
            last_name: userLoginData.last_name,
            email: userLoginData.email,
            photo: userLoginData.profile_image,
            mobileno: userLoginData.mobile_no,
            dob: userLoginData.dob
        });
    }


    render() {
        const { dob, photo, email, mobileno, first_name, last_name } = this.state;
        return (
            <View style={Styles.container,{width:300,marginBottom:50}}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>

                    <View style={Styles.cameraButtonContainer}>
                        <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd, Styles.cameraButtonView]}>

                            {!photo && <Image source={require('../../assets/images/user.png')} style={Styles.profileImage} />}
                            {photo && (
                                <Image
                                    source={{ uri: photo }}
                                    style={Styles.profileImage}
                                />
                            )}

                            <TouchableOpacity onPress={() => { this.changeAvatar() }} style={Styles.cameraButton}>
                                <Image source={require('../../assets/images/ic_camera.png')} style={Styles.cameraImage} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={FormStyle.row}>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'First Name'}
                                    autoCorrect={false}
                                    ref={(ref) => this._first_nameEntry = ref}
                                    maxLength={200}
                                    onChangeText={first_name => this.setState({ first_name })}
                                    value={first_name}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>
                        <View style={FormStyle.col2}>
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Last Name'}
                                    autoCorrect={false}
                                    value={last_name}
                                    ref={(ref) => this._last_nameEntry = ref}
                                    maxLength={200}
                                    onChangeText={last_name => this.setState({ last_name })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>
                        </View>
                    </View>

                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        <TextInput placeholderTextColor={Theme.secondaryColor}
                            placeholder={'Email'}
                            autoCorrect={false}
                            ref={(ref) => this._emailEntry = ref}
                            maxLength={200}
                            onChangeText={email => this.setState({ email })}
                            value={email}
                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                    </View>
                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        <TextInput placeholderTextColor={Theme.secondaryColor}
                            placeholder={'BirthDate'}
                            editable={false}
                            onTouchStart={() => this._showDateTimePicker()}
                            value={dob}
                            ref={(ref) => this._dobEntry = ref}
                            // onChangeText={dob => this.setState({ dob })}
                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                    </View>

                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        {/* <DateTimePickerModal
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                            isDarkModeEnabled={true}
                            textColor="white"
                            mode={'date'}
                        /> */}
                    </View>
                    <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                        <TextInput placeholderTextColor={Theme.secondaryColor}
                            placeholder={'Mobile No'}
                            autoCorrect={false}
                            ref={(ref) => this._phoneEntry = ref}
                            maxLength={10}
                            value={mobileno}
                            onChangeText={mobileno => this.setState({ mobileno })}
                            style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                    </View>
                </ScrollView>

                <View style={Styles.saveAndContinue,{marginTop:40}}>
                    <TouchableOpacity style={Styles.submitButton} onPress={() => { this.save() }}>
                        <Text style={Styles.submitButtonText}>{'Save Changes'}</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }

}

// SettingsProfile.navigationOptions = ({ navigation }) => ({
//     headerStyle: Theme.headerStyle,
//     headerLeft: <BackButton navigation={navigation} />,
//     headerTitleStyle: Theme.headerTitleStyle,
//     title: 'SETTINGS PROFILE',
//     headerRight: (<View></View>)

// })

export default SettingsProfile

const Styles = StyleSheet.create({
    saveAndContinue: {
        position: 'absolute',
        bottom: 25,
        left: 20,
        width: width - 40
    },
    submitButton: {
        borderRadius: 4,
        backgroundColor: Theme.primaryColor,
        height: isIphoneX() ? 80 : 50,
        justifyContent: isIphoneX() ? 'flex-start' : 'center',
        alignItems: 'center',
        paddingTop: isIphoneX() ? 20 : 0
    },
    submitButtonText: {
        fontSize: 16,
        fontFamily: Theme.mediumFont,
        fontWeight: '600',
        lineHeight: 19,
        letterSpacing: 0.5,
        color: Theme.white
    },
    container: {
        flex: 1,
        backgroundColor: Theme.backgroundColor,
        padding: 20
    },
    cameraButtonView: {
        width: 100,
    },
    cameraImage: {
        width: 32,
        height: 32
    },
    profileImage: {
        width: 100,
        height: 100,
        borderRadius: 50
    },
    cameraButton: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 80
    },
    cameraButtonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 30,
        marginRight: 40
    },

})