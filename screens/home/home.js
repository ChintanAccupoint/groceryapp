import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, ScrollView, RefreshControl, FlatList, StyleSheet } from "react-native";
import Theme from '../../theme/style'
import styles from './style'
import Carousel from '../../components/Carousel'
import ProductList from '../../Data/productList'
class HomeScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: '',
            status: '',
            connection_Status: '',
            context: '',
            sliders: '',
            isloading: false
        }
        // this.getslider();

    }
    getProfile() {

    }
    componentDidMount() {

        // NetInfo.addEventListener(state1 => {
        //     this.setState({ connection_Status: state1 });
        //     if (state1.isConnected && state1.isInternetReachable) {
        //         this.setState({ context: "You are online" });
        //     }
        //     else {
        //         this.setState({ context: "You are offline" });
        //     }
        // })
    }

    //commit chintan help of manoj bhai

    componentDidUpdate() {
        if (this.state.connection_Status.isConnected)
            setTimeout(() => this.setState({ context: '' }), 3000);
    }
    // getslider() {
    //     console.log("daat")
    //     // var data = {
    //     //     action: "getsliders",
    //     //     appsecret: appsecret
    //     // }
    //     // axios.post(BASE_URL + 'slider.php', data).then(result => {

    //     //     this.setState({ sliders: result.data.data })
    //     // })
    // }
    // handleforceupdate() {
    //     this.setState({ isloading: true })
    //     setTimeout(() => {
    //         this.setState({ isloading: false })
    //         this.forceUpdate();
    //     }, 2000)

    // }
    render() {
        const { navigation } = this.props;
        const { status, data, sliders, isloading } = this.state;
        return (
            <View style={Styles.view}>
            {/* {loading ? <Loading /> : null} */}
            <ScrollView >
                        <View style = {{flexDirection :"row",width:390,height:150}}>
                             <Image style={{width:400,height:150}} resizeMode={'contain'} source={require('../../assets/images/grocery-offers-banner.jpg')} />
                             <Image style={{width:200,height:100}} resizeMode={'contain'} source={require('../../assets/images/united-states-of-america.png')} />
                        </View>  
                        <View style={Styles.list}>
                      <View style={{ flex: 2.0 }}>
                    <View style={Styles.sectionHead}>
                             <Text style={Styles.sectionTitle}>{'BEST PRODUCTS'}</Text>
                             <TouchableOpacity style={Styles.seeMoreButton}>
                                <Text style={Styles.seeMoreButtonText}>see more</Text>
                                <Image style={Styles.seeMoreImage} resizeMode={'contain'} source={require('../../assets/images/ic_arrow_right.png')} />
                             </TouchableOpacity>

                    </View>
                    <View>
                    <ProductList />
                    </View>
                  
                    <View style={Styles.sectionHead}>
                        <Text style={Styles.sectionTitle}>{'NEW ARRIVALS'}</Text>
                        <TouchableOpacity style={Styles.seeMoreButton}>
                            <Text style={Styles.seeMoreButtonText}>see more</Text>
                            <Image style={Styles.seeMoreImage} resizeMode={'contain'} source={require('../../assets/images/ic_arrow_right.png')} />
                        </TouchableOpacity>
                    </View>
                    <View>
                    <ProductList />
                    </View>
                </View>
            
                </View> 
          
            </ScrollView>
            </View>     
            )
    }
}

const Styles = StyleSheet.create({
    view: {
        marginBottom: 15
    },
    list: {
        flexDirection: 'row',
        paddingRight: 15
    },
    image: {
        width: 180,
        height: 200,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#E6E8EC'
    },
    item: {
        paddingLeft: 15,
    },
    sectionHead: {
        flexDirection: 'row',
        marginBottom: 5
    },
    seeMoreButton: {
        flex: 0.5,
        paddingRight: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    seeMoreButtonText: {
        textAlign: 'right',
        fontSize: 14,
        lineHeight: 16,
        fontFamily: Theme.regularFont,
        letterSpacing: 0,
        color: '#9B9B9B',
        paddingRight: 2,
        top: 0,

    },
    seeMoreImage: {
        width: 16,
        height: 14,
        top: 1
    },
    sectionTitle: {
        fontSize: 14,
        lineHeight: 16,
        fontFamily: Theme.mediumFont,
        paddingLeft: 15,
        paddingBottom: 5,
        letterSpacing: 1,
        flex: 0.5,
        color: Theme.primaryColor
    },
    prices: {
        flexDirection: 'row'
    },
    price: {
        fontFamily: Theme.boldFont,
        fontSize: 14,
        color: Theme.primaryColor,
        letterSpacing: 0.5,
        fontWeight: '600'

    },
    price_discounted: {
        fontFamily: Theme.boldFont,
        fontSize: 13,
        color: Theme.primaryColor,
        paddingRight: 5,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        opacity: 1,
        letterSpacing: 0.5
    },
    name: {
        fontFamily: Theme.mediumFont,
        fontSize: 14,
        letterSpacing: 1,
        marginTop: 8,
        marginBottom: 5,
        color: Theme.primaryColor,
        width: 150,
        fontWeight: '500'
    }
})



export default (HomeScreen);

