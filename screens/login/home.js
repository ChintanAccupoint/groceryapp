import React, { Component,useState,useEffect } from 'react'
import FormStyle from '../../theme/form'
import Theme from '../../theme/style'
import PageStyle from './style'
import { View, Text, Keyboard, Image, TextInput, 
        TouchableOpacity, KeyboardAvoidingView, ScrollView } from 'react-native'
import auth from '@react-native-firebase/auth';

class LoginScreen extends Component {

    constructor(props) {

        super(props)
        this.state = {
            inputFocused: false,
            loginName: undefined,
            Password: undefined,
            loading: false,
            userLoginData: ''
        }
        this._loginNameEntry = undefined;
        this._PasswordEntry = undefined;
        this.keyboardBehavior = "padding";
    }

    componenDidMount() {
        this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
        this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
        this.keyboardWillHide = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)
        this.keyboardDidHide = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide)


    }


    signIn() {
        const { navigation } = this.props
        const { loginName, Password, loading } = this.state;
        if (!loading) {
            const formIsValid =
                this.validateAndSetAttribute(loginName, this._loginNameEntry) &
                this.validateAndSetAttribute(Password, this._PasswordEntry);

            if (formIsValid === 1) {
                auth()
                .signInWithEmailAndPassword(this.state.loginName, this.state.Password)
                .then(() => {
                console.log('User account created & signed in!');
                this.props.navigation.navigate('SignupScreen')
                })
                .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                alert('That email address is already in use!');
                }
    
                if (error.code === 'auth/invalid-email') {
                alert('Enterd email address is invalid!');
                }
                if(error.code === 'auth/wrong-password'){
                    alert('Enterd Password is invalid!')
                }
                console.log(error.code)
                });
                // this.showLoading()
               
            } else {
               console.log('Please enter your login informations.')
               alert('Please enter your login informations.')
            }
        }
    }

    validateInput(input) {
        if (input === undefined)
            return false
        else if (input === '')
            return false
        else if (input.trim() === '')
            return false
        else if (input === 0)
            return false
        else
            return true
    }

    Logout = () => {
        auth()
        .signOut()
        .then(() => console.log('User signed out!'));
    }

    validateAndSetAttribute(value, attribute) {
        const valid = this.validateInput(value)
        const borderBottomColor = !valid ? 'red' : '#E8E8E8';
        attribute.setNativeProps({
            style: { borderBottomColor }
        });
        return valid
    }

    render() {
        const { loading, inputFocused } = this.state
        return (
            <KeyboardAvoidingView behavior={this.keyboardBehavior} style={[FormStyle.container, PageStyle.container2]}>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} alwaysBounceVertical={true}>
                <View >
                        <View style={PageStyle.PageTitleView}>
                            <Image source={require('../../assets/images/SplashImage.png')}
                                resizeMode={'contain'}
                                style={PageStyle.Logo} />
                        </View>

                        <View >

                            { /* Email Input Form Group */}
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Username or Email'}
                                    autoCorrect={false}
                                    ref={(loginName) => this._loginNameEntry = loginName}
                                    onChangeText={(loginName) => this.setState({ loginName })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>

                            { /* Password Input Form Group */}
                            <View style={[FormStyle.formGroup, FormStyle.formGroupLessPadd]}>
                                <TextInput secureTextEntry={true}
                                    placeholderTextColor={Theme.secondaryColor}
                                    placeholder={'Password'}
                                    autoCorrect={false}
                                    ref={(Password) => this._PasswordEntry = Password}
                                    onChangeText={(Password) => this.setState({ Password })}
                                    style={[FormStyle.input, FormStyle.inputWithoutIcon]} />
                            </View>

                        </View>

                        {inputFocused === false &&
                            <TouchableOpacity style={[FormStyle.primaryButton, { marginBottom: 0, marginTop: 70 }]}
                                onPress={() => { this.signIn() }}>
                                <Text style={FormStyle.primaryButtonText}>{'SIGN IN'}</Text>
                            </TouchableOpacity>}

                        {loading && <View style={PageStyle.loadingWrap}>
                            <Loading />
                        </View>}
                    </View>
                </ScrollView>

                {inputFocused === false &&
                    <View style={PageStyle.otherOptions}>
                        <TouchableOpacity style={[FormStyle.button, { flexDirection: 'row'}]} onPress={() =>{this.props.navigation.navigate('SignupScreen')}}>
                            <Text style={[FormStyle.buttonText, { textAlign: 'center', fontWeight: '300', textDecorationLine: 'none', marginRight: 5 }]}>{'Not a member?'}</Text>
                            <Text style={[FormStyle.buttonText, { textAlign: 'center', fontWeight: '400', textDecorationLine: 'none', }]}>{'SIGN UP'}</Text>
                        </TouchableOpacity>
                    </View>}

            </KeyboardAvoidingView>



        )
    }

    keyboardDidShow = () => {
        this.setState({ inputFocused: true })
    }
    keyboardWillShow = () => {
        this.setState({ inputFocused: true })
    }
    keyboardWillHide = () => {
        this.setState({ inputFocused: false })
    }
    keyboardDidHide = () => {
        this.setState({ inputFocused: false })
    }

}


export default (LoginScreen);




