import React, { Component } from "react";
import { View, Text, TouchableOpacity, Image, } from "react-native";
import Theme from '../../../theme/style'
import FormStyle from '../../../theme/form'
import Styles from '../style'
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import * as CurrentCustomer from '../../../redux/actions/current-customer'
// import * as OrderAction from '../../../redux/actions/orders'
// import * as CartAction from '../../../redux/actions/cart'
// import * as storage from "../../../redux/actions/storage";
// import axios from 'axios';
// import config from '../../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

class CheckoutComplete extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userLoginData: '',
            order: '',
        }
        storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
            } else {
                console.log("error")
            }
        });
    }

    // complete(order) {
    //     this.props.navigation.navigate('OrdersScreen', { order })
    // }

    // UNSAFE_componentWillMount() {
    //     const { navigation } = this.props;
    //     const order = navigation.getParam('order')

    //     if (order === undefined) {
    //         navigation.goBack()
    //     } else {

    //         axios.post(BASE_URL + 'order.php', order).then(result => {
    //             this.setState({ loading: false });
    //             if (result.data.data) {
    //                 var res = result.data.data
    //                 this.setState({ order: res })
    //             }
    //         }),
    //             err => {
    //                 console.log("error")
    //             }
    //     }
    // }

    render() {
        const { loading, order } = this.state
        if (loading == true) {
            return (
                <View style={Styles.container}>
                    <View style={Styles.loadingContainer}>
                        <Image source={require('../../../assets/images/loading.gif')} resizeMode={'contain'} style={Styles.loadingImage} />
                        <Text style={Styles.loadingText}>
                            {'Payment processing please wait...'}
                        </Text>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={Styles.container}>
                    <View style={Styles.pageContentComplete}>
                        <View style={Styles.iconcCheckWrap}>
                            <Image style={Styles.iconCheck} resizeMode={'contain'}
                                source={require('../../../assets/images/ic_done.png')} />
                        </View>
                        <View style={Styles.thankyou}>
                            <Text style={Styles.thankyouText}>{'Payment Complete'}</Text>
                        </View>
                    </View>
                    <View style={Styles.desc}>
                        <Text style={Styles.descText}>{'Order ID is '}</Text>
                        <Text style={Styles.descLinkText} onPress={() => { this.complete(order) }}>
                            {order.order_no + ' '}
                        </Text>
                        <Text style={Styles.descText}>
                            {'Please check the delivery status at '}
                        </Text>
                        <Text style={Styles.descLinkText} onPress={() => { this.complete(order) }}>
                            {'Order Tracker Page'}
                        </Text>
                    </View>
                    <View style={FormStyle.bottomButton}>
                        <TouchableOpacity style={FormStyle.submitButton} onPress={() => { this.complete() }}>
                            <Text style={FormStyle.submitButtonText}>{'CONTINUE SHOPPING'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }

    }



}

// CheckoutComplete.navigationOptions = ({ navigation }) => ({
//     headerStyle: Theme.headerStyle,
//     headerTitleStyle: Theme.headerTitleStyle,
//     title: '',
//     headerLeft: (<View />),
//     headerRight: (<View />)
// })

// function mapStateToProps(state) {
//     return {
//         order: state.createOrder,
//         address: state.address,
//         currentCustomer: state.currentCustomer,
//     };
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         OrderAction: bindActionCreators(OrderAction, dispatch),
//         CurrentCustomer: bindActionCreators(CurrentCustomer, dispatch),
//         CartAction: bindActionCreators(CartAction, dispatch),
//     };
// }

export default (CheckoutComplete);


