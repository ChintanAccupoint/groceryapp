import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, SafeAreaView } from 'react-native';
import Theme from '../../../theme/style'
import BackButton from '../../../theme/back'
import FormStyle from '../../../theme/form'
import Styles from '../style'
// import * as CartAction from '../../../redux/actions/cart'
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import * as CurrentCustomer from '../../../redux/actions/current-customer'
// import * as storage from "../../../redux/actions/storage";
// import { appsecret } from '../complete';
// import config from '../../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

// const currency = 'USD'

class CheckoutPayment extends Component {

    constructor(props) {
        super(props)
        this.state = {
            payment_method: undefined,
            userLoginData: ''
        }
        // storage.storageGet("user").then((result) => {
        //     if (result != null) {
        //         this.setState({ userLoginData: JSON.parse(result) })
        //     } else {
        //         console.log("error")
        //     }
        // });
    }




    render() {
        const { payment_method } = this.state
        var paymentdata = [
            { id: "paypal" },
            { id: "cod" },
        ]
        return (
            <SafeAreaView style={Styles.container,{marginTop:100}}>
                <View style={[Styles.content, { paddingHorizontal: 10 }]}>
                    <View style={Styles.pageHeading}>
                        <Text style={Styles.pageHeadingText}>{'Payment Method'}</Text>
                    </View>
                    <View style={[Styles.options]}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                            {paymentdata && paymentdata.map((item, index) => {
                                return (
                                    <TouchableOpacity key={index} onPress={() => { this.setState({ payment_method: item.id }) }}
                                        style={[Styles.optionLeft, payment_method === item.id ? Styles.optionButtonCurrent : Styles.optionButton]}>
                                        <Image style={[Styles.optionImage, { width: '100%', height: '100%' }]}
                                            resizeMode={'contain'}
                                            source={
                                                item.id == 'paypal' ?
                                                    require('../../../assets/images/icon-paypal.png') :
                                                    item.id == 'cod' ?
                                                        require('../../../assets/images/Pay_on_Delivery-512.png') :
                                                        require('../../../assets/images/ic_card.png')
                                            }
                                        />
                                    </TouchableOpacity>
                                )
                            })}
                        </ScrollView>
                    </View>
                    <ScrollView horizontal={false} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                        <View style={Styles.cardDetails}>

                            <View style={Styles.paymentDescriptionWrapper}>
                                <Text style={Styles.paymentDescriptionText, { color: 'red' }}>
                                    {payment_method && payment_method == "paypal" ? "*Note : Due to Technical issue we do not accept online payments, please select cash on Delivery." : ""}
                                </Text>
                            </View>

                            <View style={Styles.cartPaymentTotal}>
                                <View style={Styles.subtotal}>
                                    <Text style={Styles.subtotalText}>{'Shipping'}</Text>
                                    <Text style={Styles.subtotalText2}></Text>
                                </View>
                                <View style={Styles.subtotal}>
                                    <Text style={Styles.subtotalText}>{'Subtotal'}</Text>
                                    <Text style={Styles.subtotalText2}></Text>
                                </View>
                            </View>
                            <View style={Styles.totalWrap}>
                                <Text style={Styles.totalText}>Total</Text>
                                <Text style={Styles.totalText2}></Text>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={[FormStyle.bottomButton, { marginBottom: 200 }]}>
                        <TouchableOpacity disabled={payment_method === "paypal" ? true : payment_method === "cod" ? false : true} style={payment_method == "cod" ? FormStyle.submitButton : FormStyle.submitButton1} onPress={() => { this.doCheckout() }}>
                            <Text style={FormStyle.submitButtonText}>{'CONFIRM ORDER'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </SafeAreaView>
        )
    }


    // getCartTotal() {
    //     let total = parseFloat(this.getCartSubTotal());
    //     const shippingFlatRateValue = parseFloat(this.getCartShipping())
    //     total += shippingFlatRateValue;
    //     return total.toFixed(2);
    // }

    // getCartShipping() {
    //     const { navigation } = this.props
    //     const shipping = navigation.getParam('shipping')
    //     const value = shipping.shippingCost
    //     return value.toFixed(2)
    // }

    // getCartSubTotal() {
    //     const { navigation } = this.props
    //     const shippingMethod = navigation.getParam('shipping')
    //     let total = 0
    //     total = shippingMethod.total_amt
    //     return total
    // }

    // payWithPaypal(order) {
    //     const { navigation } = this.props
    //     navigation.navigate('PaypalPayment', { order })
    // }

    // doCheckout() {

    //     const { navigation } = this.props
    //     const userLoginData = navigation.getParam('user')
    //     const shippingMethod = navigation.getParam('shipping')
    //     const { payment_method } = this.state
    //     const total = this.getCartTotal()
    //     const subtotal = this.getCartSubTotal();
    //     const shipping = this.getCartShipping()

    //     var order = {
    //         userid: userLoginData.id,
    //         action: 'placeorder',
    //         payment_type: payment_method,
    //         username: userLoginData.first_name + " " + userLoginData.last_name,
    //         subtotal: subtotal,
    //         shipment_total: shipping,
    //         address: shippingMethod.address,
    //         appsecret: appsecret
    //     }
    //     // if (payment_method === 'paypal') {
    //     //     this.payWithPaypal(order)
    //     //     // navigation.navigate('CheckoutComplete', { order })
    //     //     // }
    //     // }
    //     // else if (payment_method === 'cod') {
    //     //     navigation.navigate('CheckoutComplete', { order })
    //     // }




    //     CheckoutPayment.navigationOptions = ({ navigation }) => ({
    //         headerStyle: Theme.headerStyle,
    //         headerTitleStyle: Theme.headerTitleStyle,
    //         title: 'PAYMENT',
    //         headerLeft: <BackButton navigation={navigation} />,
    //         headerRight: (<View />)
    //     })
    // }
}
// function mapStateToProps(state) {
//     return {
//         payment: state.payment,
//         cart: state.cart,
//         currentCustomer: state.currentCustomer,
//     };
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         CartAction: bindActionCreators(CartAction, dispatch),
//         CurrentCustomer: bindActionCreators(CurrentCustomer, dispatch)
//     };
// }

export default (CheckoutPayment);
