import React, { Component } from "react"
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    ScrollView,
    FlatList,
    Dimensions,
    ImageBackground,
    RefreshControl
} from "react-native"

// import { TabView, SceneMap } from 'react-native-tab-view';
// import Animated from 'react-native-reanimated';
import Theme from '../../theme/style'
import SearchButton from '../../theme/searchButton'
import styles from './style'
import MenuButton from '../../theme/menuButton'
// import * as ProductAction from '../../redux/actions/products'
// import * as WhislistAction from '../../redux/actions/whislist'
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import NetInfo from "@react-native-community/netinfo";
// import axios from 'axios';
// import config from '../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

const width = Dimensions.get('window').width
const padd = 15
const columns = 2

class CategoryScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            index: 0,
            routes: [],
            scenes: [],
            status: '',
            connection_Status: '',
            categories: [],
            isloading: false
        }
    }
    // componentDidMount() {
    //     NetInfo.addEventListener(state1 => {
    //         console.log('Connection type', state1.type);
    //         this.setState({ connection_Status: state1 });
    //         if (state1.isConnected) {
    //             this.setState({ context: "You are online" });
    //         }
    //         else {
    //             this.setState({ context: "You are offline" });
    //         }
    //     })
    // }
    // componentDidUpdate() {
    //     if (this.state.connection_Status.isConnected)
    //         setTimeout(() => this.setState({ context: '' }), 3000);
    // }

    // UNSAFE_componentWillMount() {

    //     this.getProduct();
    // }

    getProduct() {
        this.setState({ isloading: true })
        var data = {
            action: "getCategories",
            appsecret: "judl89jkdi39k9jlj090mgs94l06kjn"
        }
        axios.post(BASE_URL + 'category.php', data).then(result => {
            this.setState({ isloading: false })
            // let routes = []
            // console.log("result", result.data.data)
            this.setState({ categories: result.data.data });
            // result.data && result.data.data.map((item) => (
            //     routes.push({
            //         key: 'tab_' + item.id,
            //         title: item.category_name.toUpperCase()
            //     })
            // ))
            // this.setState({ routes })
        }), err => {
            this.setState({ isloading: false })
            console.log("error", err)
        }
    }

    render() {
        // const { navigation } = this.props
        const { categories, isloading } = this.state;
        // const data = categories.filter(x => x.id == category.id).sort(x => x.count)
        const imageWidth = ((width - (padd * columns)) / columns) - (padd / columns)
        if (categories === undefined || categories.length == 0) {
            return (
                <View style={styles.container}>
                    <View style={styles.loadingContainer}>
                        <Image source={require('../../assets/images/loading.gif')} resizeMode={'contain'} style={styles.loadingImage} />
                        <Text style={styles.loadingText}>
                            {'please wait fetching Categories...'}
                        </Text>
                    </View>
                </View>
            )
        } else
            return (
                <View>
                    <FlatList
                        style={{ backgroundColor: '#fff' }}
                        data={categories}
                        numColumns={columns}
                        refreshControl={<RefreshControl
                            colors={["#9Bd35A", "#689F38"]}
                            // refreshing={isloading}
                            // onRefresh={this.getProduct.bind(this)}
                             />}
                        // keyExtractor={(item, index) => { return `${item.category_name}-${index}` }}
                        renderItem={({ item, index }) => {

                            return (
                                <TouchableOpacity key={index}
                                    style={styles.tabContentButton}
                                    onPress={() => { navigation.navigate('CategoryProductsScreen', { item }) }}>
                                    {
                                        item.category_image ?
                                            <ImageBackground
                                                source={{ uri: item.category_image }}
                                                resizeMode={'cover'}
                                                style={{ width: imageWidth, height: 200, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={styles.tabContentText}>{item.category_name.toUpperCase()}</Text>
                                                <Text style={styles.tabContentTextCount}>{item.count} items</Text>
                                            </ImageBackground>
                                            :
                                            <View
                                                style={{ backgroundColor: '#E6E8EC', width: imageWidth, height: 200, borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                                                <Text style={styles.tabContentText2}>{item.category_name.toUpperCase()}</Text>
                                                <Text style={styles.tabContentTextCount2}>{item.count} items</Text>
                                            </View>
                                    }

                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>

            )
    }

    // render() {
    //     return (
    //         <View>
    //             <TabView
    //                 navigationState={this.state}
    //                 renderScene={this._renderScene}
    //                 renderTabBar={this._renderTabBar}
    //                 onIndexChange={this._handleIndexChange}
    //             />
    //             {/* {
    //                 this.state.connection_Status.isConnected ?
    //                     <View style={{ backgroundColor: this.state.connection_Status && this.state.context ? 'green' : 'white' }}>
    //                         <Text style={{ fontSize: 15, fontWeight: '500', top: 3, paddingTop: 3, color: 'white', textAlign: 'center', marginBottom: 10 }}>  {this.state.context} </Text>
    //                     </View>
    //                     :
    //                     <View style={{ backgroundColor: 'red' }}>
    //                         <Text style={{ fontSize: 15, fontWeight: '500', top: 3, paddingTop: 3, color: 'white', textAlign: 'center', marginBottom: 10 }}>  {'You are Offline'}</Text>
    //                     </View>
    //             } */}
    //         </View>
    //     );
    // }



}

// CategoryScreen.navigationOptions = ({ navigation }) => ({
//     title: 'CATEGORY',
//     headerStyle: Theme.headerStyle,
//     headerTitleStyle: Theme.headerTitleStyle,
//     headerLeft: <MenuButton navigation={navigation} />,
//     headerRight: <SearchButton navigation={navigation} />
// })

export default (CategoryScreen)