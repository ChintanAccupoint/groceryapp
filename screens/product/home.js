import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    StatusBar
} from "react-native";

// import { Dropdown } from 'react-native-material-dropdown';
// import HTMLView from 'react-native-htmlview'
// import Share, { ShareSheet, Button } from 'react-native-share';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import * as ProductAction from '../../redux/actions/products'
// import * as CartAction from '../../redux/actions/cart'
import Theme from '../../theme/style'
import FormStyle from '../../theme/form'
import Styles from './style'
// import StarRating from '../../components/StarRating'
// import Heart from '../../common/heart'
// import { WHATSAPP_ICON, FACEBOOK_ICON, TWITTER_ICON, GOOGLE_PLUS_ICON, EMAIL_ICON, PINTEREST_ICON, MORE_ICON } from './icons'
// import { SliderBox } from 'react-native-image-slider-box';
// import * as storage from '../../redux/actions/storage'
// import config from '../../config'
// import axios from 'axios';
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`


class Product extends Component {

    constructor(props) {
        super(props)
        // this.attributes = [];
        // this.state = {
        //     tab_index: 0,
        //     shareSheetVisible: false,
        //     userLoginData: '',
        //     quantity: 0,
        //     value: 0
        // }
        // this.getqty()
    }
    // async getqty() {
    //     await storage.storageGet("user").then((result) => {
    //         if (result != null) {
    //             this.setState({ userLoginData: JSON.parse(result) })
    //         } else {
    //             // console.log("error", error)
    //         }
    //     });
    //     const { userLoginData } = this.state;
    //     var data = {
    //         action: "getcartcount",
    //         appsecret: appsecret,
    //         userid: userLoginData.id,
    //     }
    //     axios.post(BASE_URL + 'cart.php', data).then(result => {
    //         this.setState({ quantity: result.data.data });
    //     }),
    //         err => {
    //             console.log("error", err);
    //         }
    // }

    // render() {
    //     const { navigation } = this.props
    //     const product = navigation.getParam('item')
    //     if (product) {
    //         return this.renderProduct()
    //     }
    //     return <View></View>
    // }

    // componentWillUnmount() {
    //     StatusBar.setHidden(false, 'slide');
    // }
    
    // componentDidMount() {
    //     StatusBar.setHidden(true, 'slide');
    // }

    // renderReviewerButton(product) {
    //     const { userLoginData } = this.state;
    //     const { navigation } = this.props
    //     return (
    //         <TouchableOpacity onPress={() => { navigation.navigate('ProductReviewsScreen', { product: product, userid: userLoginData.id }) }} style={[Styles.otherDetailButton, Styles.reviewsFullRow]}>
    //             <Text style={[Styles.otherDetailButtonText, { paddingLeft: 0 }]}>{'Reviews'}</Text>
    //             <Image resizeMode={'contain'} style={Styles.otherDetailButtonImage} source={require('../../assets/images/ic_arrow_right.png')} />
    //         </TouchableOpacity>
    //     )
    // }

    // shrareButtonPress(network) {
    //     const { navigation } = this.props
    //     const product = navigation.getParam('item')
    //     this.onCancel()
    //     setTimeout(() => {
    //         Share.shareSingle({
    //             social: network,
    //             title: product.product_title,
    //             message: product.short_description,
    //             url: 'http://',
    //             subject: 'Share Link'
    //         });
    //     }, 300);
    // }


    // shareOnPress() {
    //     this.onOpen()
    // }

    // onCancel() {
    //     this.setState({ shareSheetVisible: false });
    // }

    // onOpen() {
    //     this.setState({ shareSheetVisible: true });
    // }
    // getProfile()
    // {
        
    // }

    // addToCartPress(product, goCart) {
    //     const { userLoginData } = this.state;
    //     if (userLoginData) {
    //         console.log("abc")
    //         if (goCart)
    //             this.props.navigation.navigate('CartListScreen')

    //         var data = {
    //             action: "addcart",
    //             image: product.image[0].url,
    //             appsecret: appsecret,
    //             p_id: product.id,
    //             qty: 1,
    //             userid: userLoginData.id,
    //             price: product.discount_price ? product.discount_price : product.rate,
    //             inc: true
    //         }
    //         // console.log("data", data)
    //         axios.post(BASE_URL + 'cart.php', data).then(result => {
    //             console.log("data", result.data)
    //             if (result.data.status == "success") {
    //                 this.setState({ quantity: result.data.data })
    //             }
    //         }),
    //             err => {
    //                 console.log("error", err);
    //             }
    //     }
    //     else {
    //         this.props.navigation.navigate('LoginScreen',{backTo:"HomeScreen",callHome:this.getProfile.bind(this)})
    //     }
    // }



    render() {

        // const { navigation, cart } = this.props
        // const product = navigation.getParam('item')
        // let images = []
        // for (var i = 0; i < product.image.length; i++) {
        //     images.push(product.image[i].url)
        // }

        return (
            <View style={Styles.container,{width : 300}}>
                <View style={Styles.container}>
                    <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} alwaysBounceVertical={true}>
                        {/* <View style={[Styles.productImageContainer, { height: product.rating_count > 0 ? 430 : 460 }]}>
                            {images.length > 0 && <SliderBox
                                images={images}
                                sliderBoxHeight={460}
                            />}
                        </View> */}
                        <View style={Styles.productDetails}>

                            <View style={Styles.productActions}>
                                {/* <Heart product={product} navigation={navigation} width={18} height={16} buttonStyles={Styles.productActionButton} /> */}
                                <TouchableOpacity style={Styles.productActionButton} onPress={() => { this.addToCartPress(product) }}>
                                    <Image source={require('../../assets/images/ic_cart_active.png')} resizeMode={'contain'} style={Styles.cartImage} />
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.productActionButton} onPress={() => { this.shareOnPress() }}>
                                    <Image source={require('../../assets/images/ic_share.png')} resizeMode={'contain'} style={Styles.shareImage} />
                                </TouchableOpacity>
                            </View>

                            <View style={Styles.productName}>
                                <View style={Styles.productNameTextWrap}>
                                    {/* <Text style={Styles.productNameText}>{Title}</Text> */}
                                    {/* {product.views > 0 &&
                                        // <View style={Styles.reviewStars}>
                                        //     <StarRating ratingObj={{
                                        //         ratings: parseInt(product.avgrating || 0),
                                        //         views: product.views
                                        //     }} />
                                        // </View>
                                    } */}
                                </View>
                                {/* {product.rate != '' && product.discount_price != null ?
                                    <View style={Styles.prices}>
                                        <Text style={Styles.price_discounted}>${product.rate}</Text>
                                        <Text style={Styles.productPrice}>${product.discount_price}</Text>
                                    </View>
                                    : <View style={Styles.prices}><Text style={Styles.productPrice}>${product.rate}</Text></View>
                                } */}
                            </View>
                            <View style={Styles.productContent}>
                                <View style={[Styles.productAtributesAndReviewsButton]}>
                                    {/* {this.renderReviewerButton(product)} */}
                                </View>

                                {/* <View style={Styles.productDescWrap}>
                                    <HTMLView addLineBreaks={false} value={product.long_description} stylesheet={HtmlStyles} />
                                </View> */}
                            </View>
                        </View>
                    </ScrollView>
                </View>

                <View style={Styles.header}>
                    <TouchableOpacity onPress={() => { navigation.goBack() }} style={Styles.close}>
                        <Image resizeMode={'contain'} style={Styles.closeImage} source={require('../../assets/images/icon-back.png')} />
                    </TouchableOpacity>

                    <TouchableOpacity style={Styles.cartButton}>
                        <View style={Styles.cartIconBadged}>
                            <Image resizeMode={'contain'}
                                style={Styles.cartIcon}
                                source={require('../../assets/images/ic_cart_active.png')}
                            />
                            {
                                this.state && this.state.quantity > 0 && <View style={Styles.badgeWrap}>
                                    <Text style={Styles.badgeText}>
                                        {this.state.quantity}
                                    </Text>
                                </View>
                            }
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={Styles.footer}>
                    <TouchableOpacity style={FormStyle.primaryButton} onPress={() => {
                        StatusBar.setHidden(false, 'slide');
                        this.addToCartPress(product, true);
                    }}>
                        <Text style={FormStyle.primaryButtonText}>{'BUY NOW'}</Text>
                    </TouchableOpacity>
                </View>
            </View>


        )
    }


}

export default (Product);