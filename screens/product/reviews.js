import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    StatusBar,
    Image,
    TouchableOpacity, FlatList
} from "react-native";
// import BackButton from '../../theme/back'
import Theme from '../../theme/style'
// import HTMLView from 'react-native-htmlview'
// import StarRating from '../../components/StarRating'
// import moment from 'moment'
// import Loading from "../../common/loading";
// import ModalHeader from "../../components/ModalHeader";
// import { connect } from 'react-redux';
import isIphoneX from '../../common/iphonex'
// import { bindActionCreators } from 'redux';
// import * as ProductAction from '../../redux/actions/products'
// import * as storage from '../../redux/actions/storage'
// import config from '../../config'
// import axios from 'axios';
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

class ProductReviews extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            reviewData: '',
            review: true
        }
        // this.getReview()
    }

    // UNSAFE_componentWillMount() {
    //     const { navigation } = this.props
    //     const product_id = navigation.getParam('product_id');
    //     if (navigation.getParam('res') == "success") {
    //     }
    // }

    componentDidMount() {
        StatusBar.setHidden(false, 'slide');

    }

    componentWillUnmount() {
        StatusBar.setHidden(true, 'none');
    }
    backButton = () => {
        this.props.navigation.goBack()
    }

    // async nav(add) {
    //     const { navigation } = this.props
    //     const userid = navigation.getParam('userid');
    //     const product = navigation.getParam('product');
    //     if (add == 'AddReviewScreen' && userid == null) {
    //         navigation.navigate("LoginScreen")
    //         DropDownHolder.alert('warn', '', 'You need to be login')
    //     } else {
    //         navigation.navigate('AddReviewScreen', { product_id: product.id, userid: userid })
    //     }
    // }

    // async getReview() {
    //     this.setState({ loading: true });
    //     const { navigation } = this.props
    //     const product = navigation.getParam('product');
    //     if (product.views > 0) {
    //         var data1 = {
    //             product_id: product.id,
    //             action: 'getAllreviews',
    //             appsecret: appsecret
    //         }
    //         await axios.post(BASE_URL + 'review.php', data1).then(result => {
    //             if (result.data.status = "success") {
    //                 this.setState({
    //                     loading: false,
    //                     reviewData: result.data.data,
    //                     review: false
    //                 });
    //             }
    //         }), err => {
    //             this.setState({ loading: false, review: true })
    //             console.log("error", err)
    //         }
    //     }
    //     else {
    //         this.setState({
    //             loading: false,
    //             review: true
    //         });
    //     }
    // }


    render() {
        // const { navigation } = this.props
        // const { goBack } = this.props.navigation;
        // const product_id = navigation.getParam('product_id');
        // const { loading, reviewData, review } = this.state
        return (
            <View style={Styles.container}>
                <View style={Styles.header}>
                    <View style={Styles.leftButton}>
                        <TouchableOpacity onPress={() => { goBack() }}>
                            <Image style={Styles.closeImage} resizeMode={'contain'} source={require('../../assets/images/ic_close.png')} />
                        </TouchableOpacity>
                    </View>

                    <View style={Styles.title}>
                        <Text style={Styles.headerTitle}>{'Reviews'}</Text>
                    </View>

                    <View style={Styles.rightButton}>
                        <TouchableOpacity onPress={() => { this.nav() }}>
                            <Image style={Styles.addImage} resizeMode={'contain'} source={require('../../assets/images/ic_add.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                {/* {loading && <View style={Theme.loadingWrap}><Loading /></View>} */}
                { <View style={Theme.loadingWrap}><Text style={Styles.noDataText}>{'Not reviewed yet.'}</Text></View>}
                <ScrollView showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>
                    <FlatList
                        // data={reviewData}
                        renderItem={
                            ({ item }) => {
                                return (
                                    <View style={Styles.productReviews}>
                                        <View key={'item'} style={Styles.review}>
                                            <View style={Styles.reviewAvatar}>
                                            </View>
                                            <View style={Styles.reviewBox}>
                                                <View style={Styles.reviewerRate}>
                                                    <View style={Styles.reviewer}>
                                                        <Text style={Styles.reviewerText}>{item.username}</Text>
                                                    </View>
                                                    <View style={Styles.rates}>
                                                        {/* <StarRating ratingObj={{ ratings: item.rating }} /> */}
                                                    </View>
                                                </View>
                                                {/* <View style={Styles.reviewComment}>
                                                    <HTMLView addLineBreaks={false} value={item.review_text} stylesheet={ReviewerContentHtmlStyles} />
                                                </View> */}
                                                {/* <Text style={Styles.reviewDateCreated}>{moment(item.created_at).format('MM-DD-YYYY HH:ss')}</Text> */}
                                            </View>
                                        </View>
                                    </View>
                                )
                            }}
                    />
                </ScrollView>
            </View>

        )
    }

}

function mapStateToProps(state) {
    return {
        productReviews: state.productReviews,
        currentCustomer: state.currentCustomer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        ProductAction: bindActionCreators(ProductAction, dispatch)
    };
}

const ReviewerContentHtmlStyles = StyleSheet.create({
    p: {
        fontFamily: Theme.lightFont,
        fontSize: 13.5,
        lineHeight: 24,
        color: Theme.primaryColor,
        letterSpacing: 1,
        textAlign: 'left',
        marginBottom: -25
    }

});

const Styles = StyleSheet.create({
    // Rewviews

    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    productReviews: {
        padding: 15

    },
    review: {
        paddingBottom: 10,
        marginBottom: 15,
        borderBottomColor: '#E8E8E8',
        borderBottomWidth: 1
    },
    reviewAvatar: {

    },
    reviewBox: {
        padding: 0
    },
    reviewerRate: {
        flexDirection: 'row',
        marginBottom: 10
    },
    reviewer: {
        flex: 0.7,
    },
    reviewerText: {
        fontSize: 15,
        lineHeight: 19,
        letterSpacing: 0.5,
        fontFamily: Theme.mediumFont,
        color: Theme.primaryColor
    },
    rates: {
        flex: 0.3,
        alignItems: 'flex-end'
    },
    reviewDateCreated: {
        fontSize: 11,
        lineHeight: 19,
        letterSpacing: 0.5,
        fontFamily: Theme.lightFont,
        color: Theme.primaryColor
    },
    noDataText: {
        fontSize: 15,
        lineHeight: 19,
        letterSpacing: 0.5,
        fontFamily: Theme.regularFont,
        color: Theme.primaryColor
    },
    closeImage: {
        width: 16,
        height: 16
    },
    addImage: {
        width: 20,
        height: 20
    },
    header: {
        height: isIphoneX() ? 90 : 70,
        width: '100%',
        paddingTop: isIphoneX() ? 80 : 0,
        top: isIphoneX() ? 0 : 20,
        flexDirection: 'row',
        paddingHorizontal: 0
    },
    leftButton: {
        flex: 0.33334,
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingLeft: 15
    },
    title: {
        flex: 0.33334,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 0
    },
    rightButton: {
        flex: 0.33334,
        alignItems: 'flex-end',
        paddingRight: 15,
        justifyContent: 'center'
    },
    headerTitle: {
        fontSize: 16,
        color: Theme.primaryColor,
        fontFamily: Theme.regularFont,
        lineHeight: 24
    }
});

export default (ProductReviews);