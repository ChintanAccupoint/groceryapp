import React, { Component } from "react";
import {
    View,
    Text,
    TextInput,
    FlatList,
    Keyboard,
    Image,
    TouchableOpacity,
    SafeAreaView
} from "react-native";
// import axios from 'axios';
// import config from '../../config'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`
// import * as storage from "../../redux/actions/storage";

// import * as Animatable from 'react-native-animatable'
import styles from './style'
// import Loading from '../../common/loading'
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
// import * as ProductAction from '../../redux/actions/products'
class Search extends Component {

    constructor(props) {
        super(props)
        this.state = {
            searchBarFocused: false,
            searchTerm: '',
            userLoginData: '',
            searchData: '',
            loading: false
        }
        // this.getstorageData();
    }

    // async getstorageData() {
    //     await storage.storageGet("user").then((result) => {
    //         if (result != null) {
    //             this.setState({ userLoginData: JSON.parse(result) })
    //         } else {
    //             console.log("error")
    //         }
    //     });

    // }

    // componentDidMount() {
    //     this.keyboardDidShow = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow)
    //     this.keyboardDidHide = Keyboard.addListener('keyboardDidShow', this.keyboardDidHide)
    //     this.keyboardWillShow = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow)
    //     this.keyboardWillHide = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide)
    //     setTimeout(() => {
    //         this.textInput.focus()
    //     }, 500)
    // }

    getsearchData(keyword) {
        this.setState({ loading: true });
        const { userLoginData } = this.state;
        var data = {
            action: "searchProducts",
            appsecret: "judl89jkdi39k9jlj090mgs94l06kjn",
            userid: userLoginData.id,
            text: keyword
        }
        axios.post(BASE_URL + 'getdata.php', data).then(result => {
            this.setState({ loading: false });
            if (result.data.status == "success") {
                this.setState({ searchData: result.data.data })
            }
            else {
                this.setState({ searchData: '' })
                console.log("No data found");
            }
        }), err => {
            this.setState({ loading: false });
            console.log("error", err)
        }


    }

    keyboardDidShow = () => {
        this.setState({ searchBarFocused: true })
    }

    keyboardWillShow = () => {
        this.setState({ searchBarFocused: true })
    }

    keyboardWillHide = () => {
        this.setState({ searchBarFocused: false })
    }

    keyboardDidHide = () => {
        this.setState({ searchBarFocused: false })
    }

    render() {

        const { searchBarFocused, searchData, loading } = this.state
        const { navigation } = this.props

        return (
            <SafeAreaView style={styles.searchView,{width:400,marginBottom:500}}>
                <View style={styles.searchView}>
                    <View style={styles.searchBar}>
                        {/* <Animatable.View animation="slideInRight" duration={500} style={styles.searchBarInner}> */}
                            <Image resizeMode={'contain'} style={styles.searchIcon} source={require('../../assets/images/ic_search.png')} />
                            <TextInput
                                keyboardType="default"
                                onKeyPress={this.handleKeyDown}
                                placeholder="Search product by name"
                                style={styles.searchText}
                                onChangeText={(searchTerm) => { this.setState({ searchTerm }), this.getsearchData(searchTerm) }}
                                ref={(item) => this.textInput = item}
                                returnKeyType='search'
                                autoFocus={true}
                                onSubmitEditing={() => { this.getsearchData(this.state.searchTerm) }}
                            />
                        {/* </Animatable.View> */}
                        <TouchableOpacity onPress={() => { this.props.navigation.goBack() }} style={styles.closeButton}>
                            <Image resizeMode={'contain'} style={styles.closeIcon} source={require('../../assets/images/ic_close.png')} />
                        </TouchableOpacity>
                    </View>

                    {loading && <Loading />}
                    <FlatList
                        data={searchData}
                        style={{ backgroundColor: searchBarFocused ? 'rgba(0,0,0,0.1)' : 'white' }}
                        renderItem={({ item }) =>
                            <TouchableOpacity style={styles.item} onPress={() => navigation.navigate("ProductScreen", { item })} underlayColor="white">
                                <Image style={styles.image}
                                    source={{ uri: item.image[0].url }} resizeMode={'cover'} />
                                <View style={styles.item_inner}>
                                    <Text numberOfLines={3} style={styles.name}>{item.product_title}</Text>
                                    {item.rate != '' && item.discount_price != null ?
                                        <View style={styles.prices}>
                                            <Text style={styles.price_discounted}>{'$ ' + item.rate}</Text>
                                            <Text style={styles.price}>{'$ ' + item.discount_price}</Text>
                                        </View>
                                        : <Text style={styles.price}>{'$ ' + item.rate}</Text>
                                    }

                                </View>
                            </TouchableOpacity>

                            /*      <TouchableOpacity onPress={() => { navigate('ProductScreen', { item }) }}
                                      style={styles.listItemButton}>
                                      {item.images[0].src && <Image resizeMode={'cover'} source={{ uri: item.images[0].src }} style={styles.listItemImage} />}
                                      <Text style={styles.listItemText} numberOfLines={4}>{item.name}</Text>
                                  </TouchableOpacity> */
                        }
                        // keyExtractor={(item, index) => index.toString()}
                    />
                    {/* {
                        searchData.length === 0 &&
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>{'No result found for your search.'}</Text>
                        </View>
                    } */}

                </View>
            </SafeAreaView>
        );
    }
}


// function mapStateToProps(state) {
//     return {
//         productsSearch: state.productsSearch
//     };
// }

// function mapDispatchToProps(dispatch) {
//     return {
//         ProductAction: bindActionCreators(ProductAction, dispatch),
//     };
// }

export default (Search);
