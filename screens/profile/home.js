import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView, Button } from 'react-native';

import Theme from '../../theme/style';
import styles from './style';

import OrdersScreen from '../orders/home'
// import * as storage from "../../redux/actions/storage";
// import Loading from '../../common/loading';
// import NetInfo from "@react-native-community/netinfo";

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userLoginData: '',
      lastRefresh: Date(Date.now()).toString(),
      loading: false,
      status: '',
      connection_Status: ''
    };

    this.getProfile();
  }

  componentDidMount() {
    this.props.navigation.addListener('willFocus', (route) => {
      this.getProfile();
    });
    // NetInfo.addEventListener(state1 => {
    //   console.log('Connection type', state1.type);
    //   this.setState({ connection_Status: state1 });
    //   if (state1.isConnected) {
    //     this.setState({ context: "You are online" });
    //   }
    //   else {
    //     this.setState({ context: "You are offline" });
    //   }
    // })
  }
  componentDidUpdate() {
    if (this.state.connection_Status.isConnected)
      setTimeout(() => this.setState({ context: '' }), 3000);
  }

  async getProfile() {
    // this.showLoading();
    await storage.storageGet("user").then((result) => {
      if (result != null) {
        // this.hideLoading();
        this.setState({ userLoginData: JSON.parse(result) })
      } else {
        // this.hideLoading();
        console.log("error")
      }
    });
  }



  showLoading() {
    this.setState({ loading: true })
  }

  hideLoading() {
    this.setState({ loading: false })
  }

  nav(route, requireLogin) {
    console.log("route", route)
    const { userLoginData } = this.state;
    const { navigation } = this.props;
    if (!userLoginData && requireLogin === undefined) {
      navigation.navigate('LoginScreen', { backTo: 'ProfileScreen', callHome: this.getProfile.bind(this) });
    } else {   
      navigation.navigate(route);
    }
  }

  async logout() {
    auth()
    .signOut()
    .then(() => console.log('User signed out!'));
    this.props.navigation.navigate('LoginScreen')
  }

  render() {
    const { userLoginData } = this.state;
    return (
      <View style={styles.container,{marginTop:50,marginRight:15}}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.userHeader}>
            <View style={styles.userHeaderLeft}>
              {!userLoginData.profile_image && <Image
                source={require('../../assets/images/user.png')}
                resizeMode={'cover'}
                style={styles.userIcon}
              />}
              {userLoginData.profile_image && <Image
                source={{ uri: userLoginData.profile_image }}
                resizeMode={'cover'}
                style={styles.userIcon}
              />}
            </View>
            <View style={styles.userHeaderRight}>
              <Text style={styles.userName}>
                {userLoginData
                  ? userLoginData.first_name + ' ' + userLoginData.last_name
                  : 'Guest'}
              </Text>
              {userLoginData.email && (
                <Text style={styles.userNameBottom} onPress={() => this.nav('SettingsProfileScreen')}>{userLoginData.email}</Text>
              )}
              {!userLoginData.email && (
                <TouchableOpacity onPress={() => this.nav('OrdersScreen')}>
                  <Text style={styles.userNameBottom}>
                    {'Log in to your account'}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>

          <View style={styles.buttons}>
            {/*             
                            <TouchableOpacity style={styles.button}
                                onPress={() => this.nav('NotificationScreen')}>
                                <View style={styles.buttonLf}>
                                    <Image resizeMode={'contain'} source={require('../../assets/images/ic_notification.png')} style={styles.bicon1} />
                                    <Text style={styles.btext}>{'Notification'}</Text>
                                </View>
                                <View style={styles.buttonRg}>
                                    <Image resizeMode={'contain'} source={require('../../assets/images/ic_arrow_right.png')} style={styles.bicon} />
                                </View>
                            </TouchableOpacity>
                        */}
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.props.navigation.navigate('OrderScreen')}>
              <View style={styles.buttonLf}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/ic_order.png')}
                  style={styles.bicon1}
                />
                <Text style={styles.btext}>{'Orders'}</Text>
              </View>
              <View style={styles.buttonRg}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/ic_arrow_right.png')}
                  style={styles.bicon}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button]}
              onPress={() => this.props.navigation.navigate('AddressScreen')}>
              <View style={styles.buttonLf}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/ic_location.png')}
                  style={styles.bicon1}
                />
                <Text style={styles.btext}>{'Address'}</Text>
              </View>
              <View style={styles.buttonRg}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/ic_arrow_right.png')}
                  style={styles.bicon}
                />
              </View>
            </TouchableOpacity>

            {/*
              <TouchableOpacity
                  style={[styles.button]}
                  onPress={() => this.nav('LanguageScreen', false)}>
                  <View style={styles.buttonLf}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_language.png')}
                      style={styles.bicon1}
                    />
                    <Text style={styles.btext}>{'Language'}</Text>
                  </View>
                  <View style={styles.buttonRg}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_arrow_right.png')}
                      style={styles.bicon}
                    />
                  </View>
              </TouchableOpacity>
            */}
            <TouchableOpacity
              style={[styles.button]}
              onPress={() => this.props.navigation.navigate('WhishListScreen')}>
              <View style={styles.buttonLf}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/heartWhite.png')}
                  style={styles.bicon1}
                />
                <Text style={styles.btext}>{'Whis List'}</Text>
              </View>
              <View style={styles.buttonRg}>
                <Image
                  resizeMode={'contain'}
                  source={require('../../assets/images/ic_arrow_right.png')}
                  style={styles.bicon}
                />
              </View>
            </TouchableOpacity>

            {userLoginData ? (
              <View>
                <TouchableOpacity
                  style={[styles.button, styles.blast]}
                  onPress={() => {
                    this.nav('PasswordScreen');
                  }}>
                  <View style={styles.buttonLf}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/password.png')}
                      style={styles.bicon1}
                    />
                    <Text style={styles.btext}>{'Change Password'}</Text>
                  </View>
                  <View style={styles.buttonRg}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_arrow_right.png')}
                      style={styles.bicon}
                    />
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.button, styles.blast]}
                  onPress={() => {
                    this.logout();
                  }}>
                  <View style={styles.buttonLf}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_logout.png')}
                      style={styles.bicon1}
                    />
                    <Text style={styles.btext}>{'Logout'}</Text>
                  </View>
                  <View style={styles.buttonRg}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_arrow_right.png')}
                      style={styles.bicon}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            ) : (
                <TouchableOpacity
                  style={[styles.button, styles.blast]}
                  onPress={() => {
                    this.nav('LoginScreen');
                  }}>
                  <View style={styles.buttonLf}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_logout.png')}
                      style={styles.bicon1}
                    />
                    <Text style={styles.btext}>{'Log In'}</Text>
                  </View>
                  <View style={styles.buttonRg}>
                    <Image
                      resizeMode={'contain'}
                      source={require('../../assets/images/ic_arrow_right.png')}
                      style={styles.bicon}
                    />
                  </View>
                </TouchableOpacity>
              )}
          </View>
        </ScrollView>
        {/* {
          this.state.connection_Status.isConnected ?
            <View style={{ backgroundColor: this.state.connection_Status && this.state.context ? 'green' : 'white' }}>
              <Text style={{ fontSize: 15, fontWeight: '500', top: 3, paddingTop: 3, color: 'white', textAlign: 'center', marginBottom: 10 }}>  {this.state.context} </Text>
            </View>
            :
            <View style={{ backgroundColor: 'red' }}>
              <Text style={{ fontSize: 15, fontWeight: '500', top: 3, paddingTop: 3, color: 'white', textAlign: 'center', marginBottom: 10 }}>  {'You are Offline'}</Text>
            </View>
        } */}
      </View>
    );
  }
}

// Account.navigationOptions = ({ navigation }) => ({
//   headerStyle: Theme.headerStyle,
//   headerLeft: <MenuButton navigation={navigation} />,
//   headerRight: <View />,
//   //         < TouchableOpacity style={{ marginRight: 15 }}
//   // onPress = {() => navigation.navigate(logged ? 'SettingsProfileScreen' : 'LoginScreen')}>
//   //     <Image source={require('../../assets/images/ic_setttings.png')} resizeMode={'contain'} />
//   //     </TouchableOpacity >
// });



export default (Account);
