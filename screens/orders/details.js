import React, { Component } from "react";
import {
    View,
    ScrollView,
    Text,
    TouchableOpacity, FlatList
} from "react-native";

import Theme from '../../theme/style'
import BackButton from '../../theme/back'
import Styles from './style'
// import Loading from '../../common/loading'
import style from "./style";
// import * as storage from '../../redux/actions/storage'
// import config from '../../config'
// import axios from 'axios'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

class OrderDetails extends Component {

    constructor(props) {
        super(props)
        // this.getProducts();

        // this.state = {
        //     productData: ''
        // }
    }


    // getProducts() {
    //     const { navigation } = this.props;
    //     const order = navigation.getParam('order')
    //     console.log("order");
    //     var data = {
    //         action: 'getorderedProducts',
    //         appsecret: appsecret,
    //         order_id: order.order_id
    //     }
    //     axios.post(BASE_URL + 'order.php', data).then(result => {
    //         var res = result.data.data;
    //         if (res) {
    //             this.setState({ productData: res })
    //         }

    //     }),
    //         err => {
    //             console.log("error", err)
    //         }

    // }

    render() {
        // const { navigation } = this.props
        // const order = navigation.getParam('order')
        // const { productData } = this.state
        return (
            <View style={Styles.container}>
                <View style={{ marginTop: 20 }}>
                    {/* {this.renderOrders('')} */}
                </View>

                <View style={style.section}>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionLabel}>Products</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <FlatList
                            // data={productData}
                            renderItem={({ item }) => (
                                <View style={style.line_item}>
                                    <Text style={style.line_item_text}>{item.product_title} X {item.quantity} ${item.rate}</Text>
                                </View>
                            )}
                        />
                    </View>
                </View>
                <View style={style.section}>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionLabel}>{order.payment_type === "cod" ? "Cash on Delivery" : 'Online Payment'}</Text>
                        <Text style={style.sectionParagraph}>Sub Total : ${order.sub_total}</Text>
                        <Text style={style.sectionParagraph}>Shipping  : ${order.shipping_total}</Text>
                        <Text style={style.sectionParagraph}>Total        : ${order.grand_total}</Text>
                    </View>
                </View>
                <View style={style.section}>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionLabel}>Shipping</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionParagraph}>{order.full_name}</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionParagraph}>{order.address1} </Text><Text>{order.address2}, </Text><Text>{order.city}, {order.state}, {order.country}, {order.pin_code}</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionParagraph}>{order.mobileno}</Text>
                    </View>
                </View>
                {/* <View style={style.section}>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionLabel}>Billing</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionParagraph}>{billing.first_name} {billing.last_name} {billing.company}</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionParagraph}>{billing.address_1} {billing.address_2} {billing.city} {billing.state} {billing.postcode} {billing.country}</Text>
                    </View>
                    <View style={style.sectionRow}>
                        <Text style={style.sectionParagraph}>{billing.email}</Text>
                        <Text style={style.sectionParagraph}>{billing.phone}</Text>
                    </View>
                </View> */}
            </View>
        );
    }


    renderOrders() {
        // const { navigation } = this.props;
        // const order = navigation.getParam('order')
        return <TouchableOpacity style={Styles.item}>
            <View style={Styles.left}>
                <Text style={Styles.order_no}>Order No : {order.order_no}</Text>
                <View style={Styles.dateItems}>
                    <Text style={Styles.date}>{order.date}</Text>
                    <View style={Styles.sep}></View>
                    <Text style={Styles.items}>{order.items} items</Text>
                </View>
            </View>
            <View style={Styles.right}>
                <View style={[Styles.status,
                order.payment_status == 'pending' ? Styles.pending :
                    order.payment_status == 'processing' ? Styles.processing :
                        order.payment_status == 'on-hold' ? Styles.onhold :
                            order.payment_status == 'Completed' ? Styles.completed :
                                order.payment_status == 'cancelled' ? Styles.cancelled :
                                    Styles.refunded
                ]}>
                    <Text style={Styles.status_text}>{'completed'}</Text>
                </View>
            </View>
        </TouchableOpacity>
    }
}

// OrderDetails.navigationOptions = ({ navigation }) => ({
//     headerStyle: Theme.headerStyle,
//     headerLeft: <BackButton navigation={navigation} />,
//     headerTitleStyle: Theme.headerTitleStyle,
//     title: 'Order Details',
//     headerRight: (<View></View>)
// })


export default (OrderDetails);