import React, { Component } from 'react';
import { View, ScrollView, Text, TouchableOpacity, FlatList, Image } from 'react-native';

import Theme from '../../theme/style';
import BackButton from '../../theme/back';
import Styles from './style';
// import Loading from '../../common/loading';
// import * as storage from '../../redux/actions/storage'
// import config from '../../config'
// import axios from 'axios'
// export const BASE_URL = `${config.baseurl}/`
// export const appsecret = `${config.appsecret}`

class Orders extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userLoginData: '',
      orderData: '', loading: false
    };
  }

//   componentDidMount() {
//     this.getorders();

//   }

//   orderDetails(order) {
//     const { navigation } = this.props;
//     navigation.navigate('OrderDetailScreen', { order });
//   }



  renderOrders() {
    // const { navigation } = this.props;

    // const order = navigation.getParam('order')
    return (
      <TouchableOpacity

        style={Styles.item, {width:200}}
        onPress={() => {
        //   this.orderDetails('item');
        }}>
        <View style={Styles.left}>
          <Text style={Styles.order_no}>{4}</Text>
          <View style={Styles.dateItems}>
            <Text style={Styles.date}>20-7-20</Text>
            <View style={Styles.sep} />
            <Text style={Styles.items}>3 items</Text>
          </View>
        </View>
        <View style={Styles.right}>
          <View
            style={[
              Styles.status,
              item.payment_status == 'pending'
                ? Styles.pending
                : item.payment_status == 'processing'
                  ? Styles.processing
                  : item.payment_status == 'on-hold'
                    ? Styles.onhold
                    : item.payment_status == 'completed'
                      ? Styles.completed
                      : item.payment_status == 'cancelled'
                        ? Styles.cancelled
                        : Styles.refunded,

            ]}>
            <Text style={Styles.status_text}>completed</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
    // });
  }
//   async getorders() {
//     const { navigation } = this.props;
//     await storage.storageGet("user").then((result) => {
//       if (result != null) {
//         this.setState({ userLoginData: JSON.parse(result) })
//       } else {
//         navigation.navigate('LoginScreen', { backTo: 'OrdersScreen', callHome: this.getorders.bind(this) })
//         // console.log("error", error)
//       }
//     });
//     const { userLoginData } = this.state
//     var data = {
//       userid: userLoginData.id,
//       appsecret: appsecret,
//       action: 'getAllorder'
//     }
//     axios.post(BASE_URL + 'order.php', data).then(result => {
//       var res = result.data.data;
//       if (res != undefined) {
//         this.setState({ orderData: res });
//       }
//       else {
//         this.getproducts();
//       }

//     }), err => {
//       console.log("error", err)
//     }
//   }

  renderAllOrders(res) {
    const { orderData } = this.state

    if (orderData === undefined || orderData.length == 0) {
      return (
        <View style={Styles.container, {width:200}}>
          <View style={Styles.loadingContainer}>
            <Image source={require('../../assets/images/loading.gif')} resizeMode={'contain'} style={Styles.loadingImage} />
            <Text style={Styles.loadingText}>
              {'please wait fetching your orders...'}
            </Text>
          </View>
        </View>
      )
    } else {
      return (
        <FlatList
          data={orderData}
          renderItem={({ item }) => (
            <View style={{width:200}}>
              < TouchableOpacity
                style={Styles.item}
                onPress={() => {
                //   this.orderDetails(item);
                }}>
                <View style={Styles.left}>
                  <Text style={Styles.order_no}>{item.order_no}</Text>
                  <View style={Styles.dateItems}>
                    <Text style={Styles.date}>{item.date}</Text>
                    <View style={Styles.sep} />
                    <Text style={Styles.items}>{item.items} items</Text>
                  </View>
                </View>
                <View style={Styles.right}>
                  <View
                    style={[
                      Styles.status, Styles.completed,
                      item.status == 'pending'
                        ? Styles.pending
                        : item.status == 'processing'
                          ? Styles.processing
                          : item.status == 'on-hold'
                            ? Styles.onhold
                            : item.status == 'completed'
                              ? Styles.completed
                              : item.status == 'cancelled'
                                ? Styles.cancelled
                                : Styles.refunded,

                    ]}>
                    <Text style={Styles.status_text}>{item.payment_status}</Text>
                  </View>
                </View>
              </TouchableOpacity>

            </View>
          )}
        />

      );
    }
  }
  render() {
    // const { navigation } = this.props;
    // const order = navigation.getParam('order');
    // const { orderData } = this.state;
    return (
      <View style={Styles.container, {width:200}}>
        <View>
          <View style={Styles.cartHeader}>
            <View style={Styles.cartHeaderLeft}>
              <View style={Styles.cartStatusBottom}>
                <Text style={Styles.cartStatusText2}>
                  {/* {orderData && `${orderData.length} Orders`} */}
                </Text>
              </View>
            </View>
          </View>
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <View style={Styles.orders}>
              {/* {this.renderAllOrders('')} */}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

// Orders.navigationOptions = ({ navigation }) => ({
//   headerStyle: Theme.headerStyle,
//   headerLeft: <BackButton navigation={navigation} />,
//   headerTitleStyle: Theme.headerTitleStyle,
//   title: 'MY ORDERS',
//   headerRight: <View />,
// });



export default (Orders);
