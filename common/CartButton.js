import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Image
} from "react-native";

import Badge from './badge'
import { connect } from "react-redux";
import * as storage from '../redux/actions/storage'
import config from '../config'
import axios from 'axios';
export const BASE_URL = `${config.baseurl}/`
export const appsecret = `${config.appsecret}`

const imageTint = require('../assets/images/ic_cart_active.png')
const image = require('../assets/images/ic_cart.png')
const imageStyle = { width: 25, height: 22 }

class CartButton extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userLoginData: '',
            quantity: 0
        }
        this.getqty();
    }


    async getqty() {
        await storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
            } else {
                // console.log("error", error)
            }
        });
        const { userLoginData } = this.state
        var data = {
            action: "getcartcount",
            appsecret: appsecret,
            userid: userLoginData.id,
        }
        axios.post(BASE_URL + 'cart.php', data).then(result => {
            this.setState({ quantity: result.data.data });
            // console.log("qty",qty)
        }),
            err => {
                console.log("error", err);
            }
    }

    render() {

        const { cart, focused } = this.props
        const { quantity } = this.state
        return (
            <View style={styles.tabIconStyle}>
                <Image resizeMode={'contain'} style={imageStyle} source={focused ? imageTint : image} />
                <Badge number={quantity && quantity.length > 0 ? quantity : 0} />
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        cart: state.cart
    }
}

const styles = StyleSheet.create({
    tabIconStyle: { position: 'relative' }
});

export default connect(mapStateToProps)(CartButton)
