import React, { Component } from "react";
import {
    StyleSheet,
    TouchableOpacity,
    Easing,
    Animated,
    Image
} from "react-native";

import * as Animatable from 'react-native-animatable'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as WhislistAction from '../redux/actions/whislist'
import fsManager from './fs-manager'

const empty = require('../assets/images/ic_heart_white.png')
const loved = require('../assets/images/ic_heart_red.png')
import * as storage from '../redux/actions/storage'
import config from '../config'
import axios from 'axios';
export const BASE_URL = `${config.baseurl}/`
export const appsecret = `${config.appsecret}`

class Heart extends Component {

    constructor(props) {
        super(props)
        this.state = {
            key: 1,
            exist: ''
        }
    }
    abc() {
        console.log("abc")
    }
    componentDidMount() {
        const { width, height, product, buttonStyles } = this.props
        this.animatedValue = new Animated.Value(width)
        var count = 0;
        if (count == 0) {
            count = count + 1;
            console.log("exit1qweq", product.isexist)
            this.setState({ exist: product.isexist })
        }
    }

    render() {
        const { height, product, buttonStyles } = this.props
        const animatedStyle = { width: this.animatedValue }
        const { exist } = this.state;

        return (
            <TouchableOpacity activeOpacity={1} style={[styles.button, buttonStyles]} onPress={() => { this.toggle() }}>
                <Animated.View style={[animatedStyle, { height: height }]}>
                    <Image
                        source={!exist ? empty : loved}
                        resizeMode={'contain'}
                        style={[styles.image, { width: '100%', height: '100%', marginTop: 2 }]} />
                </Animated.View>
            </TouchableOpacity>
        );
    }

    async toggle() {
        const { product, width, navigation } = this.props
        const { exist } = this.state;
        await storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
            } else {
                console.log("undefined")
            }
        });
        const { userLoginData } = this.state;
        if (userLoginData) {
            if (exist) {
                this.setState({ exist: false });
            }
            if (!exist) {
                this.setState({ exist: true });

            }
            Animated.sequence([
                Animated.spring(this.animatedValue, { toValue: width / 3, speed: 500 }),
                Animated.spring(this.animatedValue, { toValue: width, speed: 100 }),
            ]).start();

            var data = {
                action: 'addwishlist',
                appsecret: appsecret,
                product_id: product.id,
                userid: userLoginData.id,
                exist: exist ? 1 : 0
            }
            axios.post(BASE_URL + 'wishlist.php', data).then(result => {
                console.log("result", result.data);
            }),
                err => {
                    console.log("error", err);
                }
        }
        else {
            navigation.navigate('LoginScreen', { backTo: 'HomeScreen', item: product, callHome: this.abc.bind(this) })
        }

    }


}


function mapStateToProps(state) {
    return {
        whislist: state.whislist,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        WhislistAction: bindActionCreators(WhislistAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Heart)

const styles = StyleSheet.create({
    button: {

    },
    image: {

    }
});