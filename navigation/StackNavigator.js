import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import LoginScreen from "../screens/login/home";
import ProfileScreen from "../screens/profile/home";
import SignupScreen from "../screens/register/home";
import CartScreen from "../screens/cart/list/empty";
import OrderScreen from "../screens/orders/home"
import AddressScreen from "../screens/address/home"
import WhishListScreen from "../screens/wishlist/home"
import CategoryScreen from "../screens/category/home"
import HomeScreen from "../screens/home/home"
import PasswordScreen from "../screens/password/home"
import Product from "../screens/product/home"

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#9AC4F8",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
};

const MainStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="HomeScreen" component ={HomeScreen}/>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name = "SignupScreen" component = {SignupScreen}/>
      <Stack.Screen name="PasswordScreen" component ={PasswordScreen}/>
    </Stack.Navigator>
  );
};

const CategoryStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Category" component={CategoryScreen} />
      <Stack.Screen name="Product" component = {Product}/>
    </Stack.Navigator>
  );
};

const CartStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Cart" component={CartScreen} />
    </Stack.Navigator>
  );
};

const ProfileStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="OrderScreen" component ={OrderScreen}/>
      <Stack.Screen name = "AddressScreen" component = {AddressScreen}/>
      <Stack.Screen name = "WhishListScreen" component ={WhishListScreen}/>
    </Stack.Navigator>
  );
};

export { MainStackNavigator,CategoryStackNavigator,CartStackNavigator,ProfileStackNavigator };