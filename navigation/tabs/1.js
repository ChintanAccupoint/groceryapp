import React from 'react'
import { Image } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
const tabLabel = 'Shop'
const imageTint = require('../../assets/images/ic_home_active.png')
const image = require('../../assets/images/ic_home.png')
const imageStyle = { width: 22, height: 20 }

import LoginScreen from '../../screens/login/home'
import RegisterScreen from '../../screens/register/home'
// import ProfileScreen from '../../screens/profile/home'
import CategoryScreen from '../../screens/category/home'
// import CartScreen from '../../screens/cart/list/empty'
import HomeScreen from '../../screens/home/home'

const Shop = createStackNavigator(
    {
        HomeScreen,
        CategoryScreen,
        LoginScreen,
        RegisterScreen,
    }
)

const Stacks = createStackNavigator(
    {
        Shop,
        LoginScreen,
        RegisterScreen
    },
    {
        headerMode: 'none',
        mode: 'modal',
        cardStyle: {
            backgroundColor: "transparent",
            opacity: 0.99,
        },
        // tabBarOptions: {
        //     tabBarVisible: false
        // },
        tabBar: {
            visible: false
        }
    }
)

export default {
    screen: Stacks,
    navigationOptions: () => {
        return {
            tabBarLabel: tabLabel,
            tabBarIcon: ({ focused }) => (
                <Image resizeMode={'contain'} style={imageStyle} source={focused ? imageTint : image} />
            ),
        };
    },
}