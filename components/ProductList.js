import React, { Component } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Image, RefreshControl } from "react-native"
import axios from 'axios';
import config from '../config'
export const BASE_URL = `${config.baseurl}/`
export const appsecret = `${config.appsecret}`
import Loading from "../common/loading"
import Theme from '../theme/style'
import { DropDownHolder } from '../common/dropalert'
import * as storage from '../redux/actions/storage'

class ProductsView extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: '',
            data1: '',
            userLoginData: '',
            status: '',

        }
        this.getProductsDetail()
    }

    componentDidMount() {
        if (this.state.status) {
            this.props.navigation.addListener('willFocus', (route) => {
                this.getProductsDetail();
            });
        }
        else {
            this.setState({ status: false })
        }
    }

    async getProductsDetail() {
        await storage.storageGet("user").then((result) => {
            if (result != null) {
                this.setState({ userLoginData: JSON.parse(result) })
            }
            const { userLoginData } = this.state;
            var data1 = {
                action: 'getProducts',
                appsecret: appsecret,
                userid: userLoginData ? userLoginData.id : ''
            }
            var data2 = {
                action: 'NewArrivals',
                appsecret: appsecret,
                userid: userLoginData ? userLoginData.id : ''
            }
            axios.post(BASE_URL + 'getdata.php', data1)
                .then(res => {
                    this.setState({ data: res.data.data, listRefreshing: false })
                }).catch((error) => {
                    // DropDownHolder.alert('error', 'Network Error', error)
                    console.log("error", error)
                })
            axios.post(BASE_URL + 'getdata.php', data2)
                .then(res => {
                    this.setState({ data1: res.data.data, listRefreshing: false })
                }).catch((error) => {
                    // DropDownHolder.alert('error', 'Network Error', error)
                    console.log("error", error)
                })

        });
    }
    _refreshList() {
        this.setState({ listRefreshing: true })
        this.getProductsDetail();
    }
    renderProducts() {
        const { navigation, horizontal } = this.props;
        const { data, data1, listRefreshing } = this.state;

        if (data && !data1) {
            {
                listRefreshing && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Loading />
                </View>
            }
        }
        else
            return (
                <View style={{ flex: 1.0 }}>
                    <View style={Styles.sectionHead}>
                        <Text style={Styles.sectionTitle}>{'BEST PRODUCTS'}</Text>
                        <TouchableOpacity style={Styles.seeMoreButton}>
                            {/* <Text style={Styles.seeMoreButtonText}>{'see more'}</Text> */}
                            <Image style={Styles.seeMoreImage} resizeMode={'contain'} source={require('../assets/images/ic_arrow_right.png')} />
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={data}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item }) => {
                            let Image_Http_URL = { uri: item.product_cover_image };
                            return (
                                <TouchableOpacity style={[Styles.item, { marginBottom: 20 }, horizontal]} key={item.id} onPress={() => this.props.navigation.navigate('ProductScreen', { item })} underlayColor="white">
                                    {
                                        item.product_cover_image ?
                                            <Image style={Styles.image}
                                                source={Image_Http_URL} resizeMode={'cover'} />
                                            : <Image style={Styles.image}
                                                source={require('../../assets/images/india.png')} resizeMode={'cover'} />
                                    }
                                    <Text style={Styles.name}>{item.product_title}</Text>
                                    {item.rate != '' && item.discount_price != null ?
                                        <View style={Styles.prices}>
                                            <Text style={Styles.price_discounted}>{'$ ' + item.rate}</Text>
                                            <Text style={Styles.price}>{'$ ' + item.discount_price}</Text>
                                        </View>
                                        : <Text style={Styles.price}>{'$ ' + item.rate}</Text>
                                    }
                                </TouchableOpacity>
                            )
                        }}
                    />
                    <View style={Styles.sectionHead}>
                        <Text style={Styles.sectionTitle}>{'NEW ARRIVALS'}</Text>
                        <TouchableOpacity style={Styles.seeMoreButton}>
                            {/* <Text style={Styles.seeMoreButtonText}>{'see more'}</Text> */}
                            <Image style={Styles.seeMoreImage} resizeMode={'contain'} source={require('../assets/images/ic_arrow_right.png')} />
                        </TouchableOpacity>
                    </View>
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={data1}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item }) => {
                            let Image_Http_URL = { uri: item.product_cover_image };
                            return (
                                <TouchableOpacity style={[Styles.item, { marginBottom: 20 }, horizontal]} key={item.id} onPress={() => this.props.navigation.navigate('ProductScreen', { item })} underlayColor="white">
                                    {
                                        item.product_cover_image ?
                                            <Image style={Styles.image}
                                                source={Image_Http_URL} resizeMode={'cover'} />
                                            : <Image style={Styles.image}
                                                source={require('../assets/images/india.png')} resizeMode={'cover'} />
                                    }
                                    <Text style={Styles.name}>{item.product_title}</Text>
                                    {item.rate != '' && item.discount_price != null ?
                                        <View style={Styles.prices}>
                                            <Text style={Styles.price_discounted}>{'$ ' + item.rate}</Text>
                                            <Text style={Styles.price}>{'$ ' + item.discount_price}</Text>
                                        </View>
                                        : <Text style={Styles.price}>{'$ ' + item.rate}</Text>
                                    }
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            )
    }

    render() {
        const { loading, horizontal } = this.props;
        const { data } = this.state;
        return (
            <View style={Styles.view}>
                {loading ? <Loading /> : null}
                <ScrollView
                    horizontal={horizontal || false}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScroxllIndicator={false}
                    alwaysBounceVertical={false}>
                    <View style={Styles.list}>
                        {this.renderProducts()}
                        {/* {this.renderProducts1()} */}
                    </View>
                </ScrollView>
            </View>

        )
    }

}

const Styles = StyleSheet.create({
    view: {
        marginBottom: 15
    },
    list: {
        flexDirection: 'row',
        paddingRight: 15
    },
    image: {
        width: 180,
        height: 200,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#E6E8EC'
    },
    item: {
        paddingLeft: 15,
    },
    sectionHead: {
        flexDirection: 'row',
        marginBottom: 5
    },
    seeMoreButton: {
        flex: 0.5,
        paddingRight: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    seeMoreButtonText: {
        textAlign: 'right',
        fontSize: 14,
        lineHeight: 16,
        fontFamily: Theme.regularFont,
        letterSpacing: 0,
        color: '#9B9B9B',
        paddingRight: 2,
        top: 0,

    },
    seeMoreImage: {
        width: 16,
        height: 14,
        top: 1
    },
    sectionTitle: {
        fontSize: 14,
        lineHeight: 16,
        fontFamily: Theme.mediumFont,
        paddingLeft: 15,
        paddingBottom: 5,
        letterSpacing: 1,
        flex: 0.5,
        color: Theme.primaryColor
    },
    prices: {
        flexDirection: 'row'
    },
    price: {
        fontFamily: Theme.boldFont,
        fontSize: 14,
        color: Theme.primaryColor,
        letterSpacing: 0.5,
        fontWeight: '600'

    },
    price_discounted: {
        fontFamily: Theme.boldFont,
        fontSize: 13,
        color: Theme.primaryColor,
        paddingRight: 5,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        opacity: 1,
        letterSpacing: 0.5
    },
    name: {
        fontFamily: Theme.mediumFont,
        fontSize: 14,
        letterSpacing: 1,
        marginTop: 8,
        marginBottom: 5,
        color: Theme.primaryColor,
        width: 150,
        fontWeight: '500'
    }
})

export default ProductsView