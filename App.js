import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import TabNavigator from './navigation/TabNavigator'

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <TabNavigator />
    </NavigationContainer>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userIcon: {
    width: 30,
    height: 30,
    borderRadius: 30
},
listIcon: {
  width: 30,
  height: 30
},
});
