import React, { Component } from "react";
import Theme from '../theme/style'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList, Image, RefreshControl } from "react-native"
import { Card, CardTitle,CardContent, CardAction, CardButton, CardImage } from 'react-native-cards';


class productList extends Component {
    constructor(props){
        super(props)
    }
    data(){
        console.log("Clicked")
    }
    render() {
        return (
            <View style={Styles.view}>
                {/* {loading ? <Loading /> : null} */}
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScroxllIndicator={false}
                    alwaysBounceVertical={false}>
                    <TouchableOpacity onPress = {() => this.data( )}   >
                         <Card onPress={() => this.props.navigation.navigate('Product')} >
                            <CardImage 
                            source={require('../assets/images/product.jpg')}
                            />
                            <CardTitle
                            subtitle="Number 6"
                            />
                            <CardContent text="Clifton, Western Cape" />
                            <CardAction 
                            separator={true} 
                            inColumn={false}>
                            <CardButton
                                onPress={() => {}}
                                title="Add To Cart"
                                color="#FEB557"
                            />
                            <CardButton
                                onPress={() => {}}
                                title="Add To Like"
                                color="#FEB557"
                            />
                            </CardAction>
                        </Card>
                        </TouchableOpacity>
                        <Card>
                            <CardImage 
                            source={require('../assets/images/product.jpg')}
                            />
                            <CardTitle
                            subtitle="Number 6"
                            />
                            <CardContent text="Clifton, Western Cape" />
                            <CardAction 
                            separator={true} 
                            inColumn={false}>
                            <CardButton
                                onPress={() => {}}
                                title="Add To Cart"
                                color="#FEB557"
                            />
                            <CardButton
                                onPress={() => {}}
                                title="Add to Like"
                                color="#FEB557"
                            />
                            </CardAction>
                        </Card>

                        <Card>
                            <CardImage 
                            source={require('../assets/images/product.jpg')}
                            />
                            <CardTitle
                            subtitle="Number 6"
                            />
                            <CardContent text="Clifton, Western Cape" />
                            <CardAction 
                            separator={true} 
                            inColumn={false}>
                            <CardButton
                                onPress={() => {}}
                                title="Add To Cart"
                                color="#FEB557"
                            />
                            <CardButton
                                onPress={() => {}}
                                title="Add To Like"
                                color="#FEB557"
                            />
                            </CardAction>
                        </Card>

                </ScrollView>
            </View>

        )
    }

}

const Styles = StyleSheet.create({
    view: {
        marginBottom: 15
    },
    list: {
        flexDirection: 'row',
        paddingRight: 15
    },
    image: {
        width: 180,
        height: 200,
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#E6E8EC'
    },
    item: {
        paddingLeft: 15,
    },
    sectionHead: {
        flexDirection: 'row',
        marginBottom: 5
    },
    seeMoreButton: {
        flex: 0.5,
        paddingRight: 5,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    seeMoreButtonText: {
        textAlign: 'right',
        fontSize: 14,
        lineHeight: 16,
        fontFamily: Theme.regularFont,
        letterSpacing: 0,
        color: '#9B9B9B',
        paddingRight: 2,
        top: 0,

    },
    seeMoreImage: {
        width: 16,
        height: 14,
        top: 1
    },
    sectionTitle: {
        fontSize: 14,
        lineHeight: 16,
        fontFamily: Theme.mediumFont,
        paddingLeft: 15,
        paddingBottom: 5,
        letterSpacing: 1,
        flex: 0.5,
        color: Theme.primaryColor
    },
    prices: {
        flexDirection: 'row',
    },
    price: {
        fontFamily: Theme.boldFont,
        fontSize: 14,
        marginBottom : 40,
        color: Theme.primaryColor,
        letterSpacing: 0.5,
        fontWeight: '600',
        marginLeft : 20

    },
    price_discounted: {
        fontFamily: Theme.boldFont,
        fontSize: 13,
        color: Theme.primaryColor,
        paddingRight: 5,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
        opacity: 1,
        letterSpacing: 0.5,
        marginLeft : 20
    },
    name: {
        fontFamily: Theme.mediumFont,
        fontSize: 14,
        letterSpacing: 1,
        marginLeft : 20,
        color: Theme.primaryColor,
        width: 150,
        fontWeight: '500'
    }
})


export default productList